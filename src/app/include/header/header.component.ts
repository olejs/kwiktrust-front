import {ChangeDetectorRef, Component, NgZone, OnInit} from '@angular/core';
import { AuthHelper } from '../../auth/helpers/auth.helper';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public isLogged;

  constructor(private authHelper: AuthHelper, private ngZone: NgZone) { }

  ngOnInit() {
    this.isLogged = this.authHelper.isLoggedIn();
  }

  public logOut() {
    this.authHelper.logout();
  }
}
