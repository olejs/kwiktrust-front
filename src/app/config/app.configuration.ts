const host = 'localhost';
const port = '3400';
const scheme = 'http';

export const ApiConfiguration = {
  endpoint: `${scheme}://${host}:${port}/api/`
};
