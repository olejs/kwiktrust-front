import { Component, OnInit } from '@angular/core';
import {FormControl, FormControlDirective, FormGroup, Validators} from '@angular/forms';
import {AuthHelper} from '../helpers/auth.helper';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public passwordType = 'password';
  private _isError = false;
  private regId: number;
  private _statusMsg: any = {} as any;
  public signupForm = new FormGroup({
    firstname: new FormControl('', [Validators.required, Validators.minLength(2)]),
    lastname: new FormControl('', [Validators.required, Validators.minLength(2)]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(2)])
  });
  constructor(private authHelper: AuthHelper) { }

  ngOnInit() {
    this.authHelper.onAuthStatus.subscribe(r => {
      if (r && r.id === this.regId && r.response) {
        if (r.response.statusCode > 301) {
          this._isError = true;
          this._statusMsg = r.response.data;
        } else {
          this.signupForm.reset();
          this._statusMsg = {} as any;
          alert(r.response.statusText);
        }
      }
    });
  }

  public togglePasswordType() {
    if (this.passwordType === 'password') {
      this.passwordType = 'text';
    } else {
      this.passwordType = 'password';
    }
  }

  public signup() {
    this.regId = Math.random();
    this.authHelper.fakeForm = this.signupForm;
    this.authHelper.register(this.regId);
  }

  public preventDefault(event) {
    event.preventDefault();
  }


  get isError(): boolean {
    return this._isError;
  }

  get statusMsg(): any {
    return this._statusMsg;
  }
}
