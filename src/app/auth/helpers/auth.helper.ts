
import {Injectable} from '@angular/core';
import {ApiProvider} from '../../provider/api.service';
import {FormGroup} from '@angular/forms';
import {Subject} from 'rxjs/index';
import {Router} from '@angular/router';
import * as moment from 'moment';
import {promise} from 'selenium-webdriver';
import Promise = promise.Promise;
@Injectable({
  providedIn: 'root'
})


export class AuthHelper {
  public fakeForm: FormGroup;
  private _onAuthStatus: Subject<{id: number, response: any}> = new Subject<{id: number, response: any}>();
  constructor(private api: ApiProvider, private router: Router) {}

  public login(id?: number) {
    if (!this.fakeForm) {
      console.error('Form value is required');
      return;
    }

    this.api.getUrl('auth/login', this.fakeForm.value, 'post', false).subscribe(r => {

      if (r && r.statusText) {
        this.setSession(r.data);
        this._onAuthStatus.next({id, response: r});
      }
    }, error => {
      this._onAuthStatus.next({id, response: error});
    });

  }

  public register(id?: number) {
    if (!this.fakeForm) {
      console.error('Form value is required');
      return;
    }
    this.api.getUrl('auth/signup', this.fakeForm.value, 'post', false).subscribe(r => {
      if (r && r.statusText) {
        this._onAuthStatus.next({id, response: r});
      }
    }, error => {
      this._onAuthStatus.next({id, response: error});
    });
  }

  get onAuthStatus(): Subject<{ id: number; response: any }> {
    return this._onAuthStatus;
  }

  private setSession(authResult) {
    const expiresAt = moment().add(authResult.expires, 'second');
    localStorage.setItem('token', authResult.token);
    localStorage.setItem('expires', JSON.stringify(expiresAt.valueOf()) );
  }

  private getExpiration() {
    const expiration = localStorage.getItem('expires');
    const expiresAt = JSON.parse(expiration);
    return moment(expiresAt);
  }

  public isLoggedIn() {
    return moment().isBefore(this.getExpiration());
  }

  public isLoggedOut() {
    return !this.isLoggedIn();
  }

  public logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('expires');
  }
}
