(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["auth-auth-module"],{

/***/ "./src/app/auth/auth.module.ts":
/*!*************************************!*\
  !*** ./src/app/auth/auth.module.ts ***!
  \*************************************/
/*! exports provided: AuthModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthModule", function() { return AuthModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login/login.component */ "./src/app/auth/login/login.component.ts");
/* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./register/register.component */ "./src/app/auth/register/register.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _include_include_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../include/include.module */ "./src/app/include/include.module.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var route = [{
        path: '',
        component: _login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"]
    }, {
        path: 'login',
        component: _login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"]
    }, {
        path: 'signup',
        component: _register_register_component__WEBPACK_IMPORTED_MODULE_3__["RegisterComponent"],
    }];
var AuthModule = /** @class */ (function () {
    function AuthModule() {
    }
    AuthModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"], _register_register_component__WEBPACK_IMPORTED_MODULE_3__["RegisterComponent"]],
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(route),
                _include_include_module__WEBPACK_IMPORTED_MODULE_5__["IncludeModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatGridListModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]
            ],
            exports: [_login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"], _register_register_component__WEBPACK_IMPORTED_MODULE_3__["RegisterComponent"]]
        })
    ], AuthModule);
    return AuthModule;
}());



/***/ }),

/***/ "./src/app/auth/login/login.component.html":
/*!*************************************************!*\
  !*** ./src/app/auth/login/login.component.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<section [class.bg-bg]=\"true\">\n  <div class=\"container\">\n    <mat-card [class.login-box]=\"true\">\n      <form (submit)=\"preventDefault($event)\" [formGroup]=\"loginForm\" (ngSubmit)=\"login()\">\n\n    <mat-card-content>\n        <div class=\"top-card\">\n          <h4 [class.text-dark]=\"true\" [class.text-center]=\"true\">Log in to Kwiktrust</h4>\n        </div>\n\n\n        <div class=\"middle-card\">\n          <mat-form-field>\n            <mat-label [class.danger]=\"isError && statusMsg.email\">{{isError && statusMsg.email?statusMsg.email:'Email'}}</mat-label>\n            <input matInput placeholder=\"Email\" type=\"email\" formControlName=\"email\">\n          </mat-form-field>\n\n          <mat-form-field>\n            <mat-label [class.danger]=\"isError && statusMsg.password\">{{isError && statusMsg.password?statusMsg.password:'Password'}}</mat-label>\n            <input matInput placeholder=\"*********\" [type]=\"passwordType\" formControlName=\"password\">\n           <mat-icon matSuffix [class.text-light]=\"true\" [class.pointer]=\"true\" (click)=\"togglePasswordType()\">visibility</mat-icon>\n\n          </mat-form-field>\n\n        </div>\n\n        <div class=\"bottom-card\">\n          <button mat-button type=\"submit\" [disabled]=\"!loginForm.valid\" [class.login-button]=\"true\" [class.light]=\"true\"\n                  [class.round]=\"true\" [class.secondary-bg]=\"true\">LOG IN</button>\n        </div>\n      </mat-card-content>\n      </form>\n\n    </mat-card>\n  </div>\n</section>\n<app-footer></app-footer>\n"

/***/ }),

/***/ "./src/app/auth/login/login.component.scss":
/*!*************************************************!*\
  !*** ./src/app/auth/login/login.component.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "app-footer {\n  position: fixed;\n  width: 100%;\n  bottom: 29px; }\n\nsection {\n  height: calc(100% - 72px);\n  width: 100%; }\n\nsection .container {\n    display: flex; }\n\nsection .container mat-card {\n      max-width: 556px;\n      height: 100%;\n      display: block;\n      margin: 120px auto;\n      padding: 40px 0;\n      max-height: 360px;\n      width: 100%; }\n\nsection .container .login-button {\n      width: 80%;\n      margin: 0 auto;\n      display: block;\n      font-size: 16px;\n      font-weight: bold;\n      font-family: 'Lato',sans-serif;\n      box-shadow: 0 10px 20px 0 rgba(170, 202, 65, 0.5); }\n\nsection .container mat-form-field {\n      width: 100%; }\n\nsection .container .middle-card, section .container .top-card, section .container .bottom-card {\n      width: 328px;\n      display: block;\n      margin: auto; }\n\nsection .container .middle-card {\n      margin: 68px auto 40px; }\n\nsection .container mat-form-field:first-child {\n      margin-bottom: 20px; }\n\nsection .container h4 {\n      font-family: 'Lato',sans-serif;\n      font-size: 18px;\n      padding: 0;\n      margin: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29sZWpzL1Byb2plY3RzL2tlbHdpLWZyb250ZW5kL3NyYy9hcHAvYXV0aC9sb2dpbi9sb2dpbi5jb21wb25lbnQuc2NzcyIsIi9ob21lL29sZWpzL1Byb2plY3RzL2tlbHdpLWZyb250ZW5kL3NyYy90aGVtZS9fbWl4aW5zLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxlQUFjO0VBQ2QsV0FBVTtFQUNWLFlBQVcsRUFBQTs7QUFHYjtFQUNFLHlCQUF3QjtFQUN4QixXQUFVLEVBQUE7O0FBRlo7SUFJSSxhQUFZLEVBQUE7O0FBSmhCO01BTU0sZ0JBQWU7TUFDZixZQUFXO01BQ1gsY0FBYTtNQUNiLGtCQUFpQjtNQUNqQixlQUFjO01BQ2QsaUJBQWdCO01BQ2hCLFdBQVUsRUFBQTs7QUFaaEI7TUFvQk0sVUFBUztNQUNULGNBQWE7TUFDYixjQUFhO01BQ2IsZUFBYztNQUNkLGlCQUFnQjtNQUNoQiw4QkFBNkI7TUNqQmpDLGlERGtCOEQsRUFBQTs7QUExQmhFO01BNkJNLFdBQVUsRUFBQTs7QUE3QmhCO01Ba0NNLFlBQVc7TUFDWCxjQUFhO01BQ2IsWUFBVyxFQUFBOztBQXBDakI7TUF1Q00sc0JBQXFCLEVBQUE7O0FBdkMzQjtNQTRDUSxtQkFBa0IsRUFBQTs7QUE1QzFCO01BaURNLDhCQUE2QjtNQUM3QixlQUFjO01BQ2QsVUFBUztNQUNULFNBQVEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2F1dGgvbG9naW4vbG9naW4uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0ICcuLi8uLi8uLi90aGVtZS9taXhpbnMnO1xuXG5hcHAtZm9vdGVye1xuICBwb3NpdGlvbjpmaXhlZDtcbiAgd2lkdGg6MTAwJTtcbiAgYm90dG9tOjI5cHg7XG5cbn1cbnNlY3Rpb257XG4gIGhlaWdodDpjYWxjKDEwMCUgLSA3MnB4KTtcbiAgd2lkdGg6MTAwJTtcbiAgLmNvbnRhaW5lcntcbiAgICBkaXNwbGF5OmZsZXg7XG4gICAgbWF0LWNhcmR7XG4gICAgICBtYXgtd2lkdGg6NTU2cHg7XG4gICAgICBoZWlnaHQ6MTAwJTtcbiAgICAgIGRpc3BsYXk6YmxvY2s7XG4gICAgICBtYXJnaW46MTIwcHggYXV0bztcbiAgICAgIHBhZGRpbmc6NDBweCAwO1xuICAgICAgbWF4LWhlaWdodDozNjBweDtcbiAgICAgIHdpZHRoOjEwMCU7XG4gICAgfVxuXG5cblxuXG5cbiAgICAubG9naW4tYnV0dG9ue1xuICAgICAgd2lkdGg6ODAlO1xuICAgICAgbWFyZ2luOjAgYXV0bztcbiAgICAgIGRpc3BsYXk6YmxvY2s7XG4gICAgICBmb250LXNpemU6MTZweDtcbiAgICAgIGZvbnQtd2VpZ2h0OmJvbGQ7XG4gICAgICBmb250LWZhbWlseTonTGF0bycsc2Fucy1zZXJpZjtcbiAgICAgIEBpbmNsdWRlIGJveC1zaGFkb3coIDAgMTBweCAyMHB4IDAgcmdiYSgxNzAsIDIwMiwgNjUsIDAuNSkpO1xuICAgIH1cbiAgICBtYXQtZm9ybS1maWVsZHtcbiAgICAgIHdpZHRoOjEwMCU7XG4gICAgfVxuXG5cbiAgICAubWlkZGxlLWNhcmQsLnRvcC1jYXJkLC5ib3R0b20tY2FyZHtcbiAgICAgIHdpZHRoOjMyOHB4O1xuICAgICAgZGlzcGxheTpibG9jaztcbiAgICAgIG1hcmdpbjphdXRvO1xuICAgIH1cbiAgICAubWlkZGxlLWNhcmR7XG4gICAgICBtYXJnaW46NjhweCBhdXRvIDQwcHg7XG4gICAgfVxuXG4gICAgbWF0LWZvcm0tZmllbGR7XG4gICAgICAmOmZpcnN0LWNoaWxke1xuICAgICAgICBtYXJnaW4tYm90dG9tOjIwcHg7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaDR7XG4gICAgICBmb250LWZhbWlseTonTGF0bycsc2Fucy1zZXJpZjtcbiAgICAgIGZvbnQtc2l6ZToxOHB4O1xuICAgICAgcGFkZGluZzowO1xuICAgICAgbWFyZ2luOjA7XG4gICAgfVxuICB9XG59XG4iLCJAbWl4aW4gY29tbW9uQmcoJHdpZHRoOjEwMCUpe1xuICB3aWR0aDokd2lkdGg7XG4gIGJhY2tncm91bmQtc2l6ZTpjb3ZlcjtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjpjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0Om5vLXJlcGVhdDtcbn1cblxuXG5AbWl4aW4gYm9yZGVyLXJhZGl1cygkdmFsdWUpe1xuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6JHZhbHVlO1xuICAtbW96LWJvcmRlci1yYWRpdXM6JHZhbHVlO1xuICBib3JkZXItcmFkaXVzOiR2YWx1ZTtcbn1cbkBtaXhpbiBib3gtc2hhZG93KCR2YWx1ZSl7XG4gIC13ZWJraXQtYm94LXNoYWRvdzogJHZhbHVlO1xuICAtbW96LWJveC1zaGFkb3c6ICR2YWx1ZTtcbiAgYm94LXNoYWRvdzogJHZhbHVlO1xufVxuXG5cbkBtaXhpbiBjdXN0b20tdHJhbnNpdGlvbigkdmFsdWUpe1xuICB0cmFuc2l0aW9uOiAkdmFsdWU7XG4gIC13ZWJraXQtdHJhbnNpdGlvbjogJHZhbHVlO1xuICAtbW96LXRyYW5zaXRpb246ICR2YWx1ZTtcbiAgLW1zLXRyYW5zaXRpb246ICR2YWx1ZTtcbn1cblxuQG1peGluIGN1c3RvbS10cmFuc2Zvcm0oJHZhbHVlKXtcbiAgLXdlYmtpdC10cmFuc2Zvcm06ICR2YWx1ZTtcbiAgLW1vei10cmFuc2Zvcm06ICR2YWx1ZTtcbiAgLW1zLXRyYW5zZm9ybTogJHZhbHVlO1xuICAtby10cmFuc2Zvcm06ICR2YWx1ZTtcbiAgdHJhbnNmb3JtOiAkdmFsdWU7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/auth/login/login.component.ts":
/*!***********************************************!*\
  !*** ./src/app/auth/login/login.component.ts ***!
  \***********************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _helpers_auth_helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../helpers/auth.helper */ "./src/app/auth/helpers/auth.helper.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginComponent = /** @class */ (function () {
    function LoginComponent(authHelper, router) {
        this.authHelper = authHelper;
        this.router = router;
        this.passwordType = 'password';
        this._isError = false;
        this.loginId = {};
        this._statusMsg = {};
        this.loginForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(2)])
        });
        if (this.authHelper.isLoggedIn()) {
            this.router.navigate(['dashboard']);
        }
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authHelper.onAuthStatus.subscribe(function (r) {
            if (r && r.id === _this.loginId && r.response) {
                if (r.response.statusCode > 301) {
                    _this._isError = true;
                    _this._statusMsg = r.response.data;
                }
                else {
                    _this.loginForm.reset();
                    _this._statusMsg = {};
                    _this.router.navigate(['dashboard']);
                    // alert(r.response.statusText);
                }
            }
        });
    };
    LoginComponent.prototype.togglePasswordType = function () {
        if (this.passwordType === 'password') {
            this.passwordType = 'text';
        }
        else {
            this.passwordType = 'password';
        }
    };
    LoginComponent.prototype.login = function () {
        this.loginId = Math.random();
        this.authHelper.fakeForm = this.loginForm;
        this.authHelper.login(this.loginId);
    };
    LoginComponent.prototype.preventDefault = function (event) {
        event.preventDefault();
    };
    Object.defineProperty(LoginComponent.prototype, "isError", {
        get: function () {
            return this._isError;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LoginComponent.prototype, "statusMsg", {
        get: function () {
            return this._statusMsg;
        },
        enumerable: true,
        configurable: true
    });
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/auth/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/auth/login/login.component.scss")]
        }),
        __metadata("design:paramtypes", [_helpers_auth_helper__WEBPACK_IMPORTED_MODULE_2__["AuthHelper"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/auth/register/register.component.html":
/*!*******************************************************!*\
  !*** ./src/app/auth/register/register.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<section [class.bg-bg]=\"true\">\n  <div class=\"container\">\n    <mat-card [class.login-box]=\"true\">\n      <form (submit)=\"preventDefault($event)\" [formGroup]=\"signupForm\" (ngSubmit)=\"signup()\">\n\n        <mat-card-content>\n          <div class=\"top-card\">\n            <h4 [class.text-dark]=\"true\" [class.text-center]=\"true\">Sign Up to Kwiktrust</h4>\n            <h6 [class.text-light]=\"true\" [class.text-center]=\"true\">Create your account by filling the form below</h6>\n          </div>\n\n\n          <div class=\"middle-card\">\n            <mat-grid-list rowHeight=\"75px\" cols=\"2\">\n              <mat-grid-tile>\n                <mat-form-field class=\"right-space\">\n                  <mat-label [class.danger]=\"isError && statusMsg.firstname\">{{isError && statusMsg.firstname?statusMsg.firstname:'First Name'}}</mat-label>\n                  <input matInput placeholder=\"First Name\" type=\"text\" formControlName=\"firstname\">\n                </mat-form-field>\n\n              </mat-grid-tile>\n              <mat-grid-tile>\n                <mat-form-field class=\"left-space\">\n                  <mat-label [class.danger]=\"isError && statusMsg.lastname\">{{isError && statusMsg.lastname?statusMsg.lastname:'Last Name'}}</mat-label>\n                  <input matInput placeholder=\"Last Name\" type=\"text\" formControlName=\"lastname\">\n                </mat-form-field>\n\n              </mat-grid-tile>\n              <mat-grid-tile colspan=\"2\">\n                <mat-form-field>\n                  <mat-label [class.danger]=\"isError && statusMsg.email\">{{isError && statusMsg.email?statusMsg.email:'Email'}}</mat-label>\n                  <input matInput placeholder=\"Email\" type=\"email\" formControlName=\"email\">\n                </mat-form-field>\n\n              </mat-grid-tile>\n              <mat-grid-tile colspan=\"2\">\n\n                <mat-form-field>\n                  <mat-label [class.danger]=\"isError && statusMsg.password\">{{isError && statusMsg.password?statusMsg.password:'Password'}}</mat-label>\n                  <input matInput placeholder=\"*********\" [type]=\"passwordType\" formControlName=\"password\">\n                  <mat-icon matSuffix [class.text-light]=\"true\" [class.pointer]=\"true\" (click)=\"togglePasswordType()\">visibility</mat-icon>\n\n                </mat-form-field>\n              </mat-grid-tile>\n            </mat-grid-list>\n\n\n\n          </div>\n\n          <div class=\"bottom-card\">\n            <button mat-button type=\"submit\" [disabled]=\"!signupForm.valid\" [class.login-button]=\"true\" [class.light]=\"true\"\n                    [class.round]=\"true\" [class.secondary-bg]=\"true\">CREATE ACCOUNT</button>\n          </div>\n        </mat-card-content>\n      </form>\n\n    </mat-card>\n  </div>\n</section>\n<app-footer></app-footer>\n"

/***/ }),

/***/ "./src/app/auth/register/register.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/auth/register/register.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "app-footer {\n  position: fixed;\n  width: 100%;\n  bottom: 29px; }\n\nsection {\n  height: calc(100% - 72px);\n  width: 100%; }\n\nsection .container {\n    display: flex; }\n\nsection .container mat-card {\n      max-width: 556px;\n      height: 100%;\n      display: block;\n      margin: 120px auto;\n      padding: 40px 0 54px;\n      max-height: 482px;\n      width: 100%; }\n\nsection .container .left-space {\n      padding-left: 25px;\n      width: calc(100% - 25px); }\n\nsection .container .right-space {\n      padding-right: 25px;\n      width: calc(100% - 25px); }\n\nsection .container .login-button {\n      width: 80%;\n      margin: 0 auto;\n      height: 53px;\n      display: block;\n      font-size: 16px;\n      font-weight: bold;\n      font-family: 'Lato',sans-serif;\n      box-shadow: 0 10px 20px 0 rgba(170, 202, 65, 0.5); }\n\nsection .container mat-form-field {\n      width: 100%; }\n\nsection .container .middle-card, section .container .top-card, section .container .bottom-card {\n      width: 328px;\n      display: block;\n      margin: auto; }\n\nsection .container .middle-card {\n      margin: 35px auto 70px; }\n\nsection .container mat-form-field:first-child {\n      margin-bottom: 20px; }\n\nsection .container h4 {\n      font-family: 'Lato',sans-serif;\n      font-size: 18px;\n      padding: 0;\n      margin: 0; }\n\nsection .container h6 {\n      font-family: 'Lato',sans-serif;\n      font-size: 16px;\n      margin: 0;\n      margin-top: 14px;\n      font-weight: bold; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29sZWpzL1Byb2plY3RzL2tlbHdpLWZyb250ZW5kL3NyYy9hcHAvYXV0aC9yZWdpc3Rlci9yZWdpc3Rlci5jb21wb25lbnQuc2NzcyIsIi9ob21lL29sZWpzL1Byb2plY3RzL2tlbHdpLWZyb250ZW5kL3NyYy90aGVtZS9fbWl4aW5zLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxlQUFjO0VBQ2QsV0FBVTtFQUNWLFlBQVcsRUFBQTs7QUFHYjtFQUNFLHlCQUF3QjtFQUN4QixXQUFVLEVBQUE7O0FBRlo7SUFJSSxhQUFZLEVBQUE7O0FBSmhCO01BTU0sZ0JBQWU7TUFDZixZQUFXO01BQ1gsY0FBYTtNQUNiLGtCQUFpQjtNQUNqQixvQkFBbUI7TUFDbkIsaUJBQWdCO01BQ2hCLFdBQVUsRUFBQTs7QUFaaEI7TUFpQk0sa0JBQWlCO01BQ2pCLHdCQUF1QixFQUFBOztBQWxCN0I7TUFxQk0sbUJBQWtCO01BQ2xCLHdCQUF1QixFQUFBOztBQXRCN0I7TUE2Qk0sVUFBUztNQUNULGNBQWE7TUFDYixZQUFXO01BQ1gsY0FBYTtNQUNiLGVBQWM7TUFDZCxpQkFBZ0I7TUFDaEIsOEJBQTZCO01DM0JqQyxpREQ0QjhELEVBQUE7O0FBcENoRTtNQXVDTSxXQUFVLEVBQUE7O0FBdkNoQjtNQTRDTSxZQUFXO01BQ1gsY0FBYTtNQUNiLFlBQVcsRUFBQTs7QUE5Q2pCO01BaURNLHNCQUFxQixFQUFBOztBQWpEM0I7TUFzRFEsbUJBQWtCLEVBQUE7O0FBdEQxQjtNQTJETSw4QkFBNkI7TUFDN0IsZUFBYztNQUNkLFVBQVM7TUFDVCxTQUFRLEVBQUE7O0FBOURkO01Ba0VNLDhCQUE2QjtNQUM3QixlQUFjO01BQ2QsU0FBUTtNQUNSLGdCQUFlO01BQ2YsaUJBQWdCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9hdXRoL3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCAnLi4vLi4vLi4vdGhlbWUvbWl4aW5zJztcblxuYXBwLWZvb3RlcntcbiAgcG9zaXRpb246Zml4ZWQ7XG4gIHdpZHRoOjEwMCU7XG4gIGJvdHRvbToyOXB4O1xuXG59XG5zZWN0aW9ue1xuICBoZWlnaHQ6Y2FsYygxMDAlIC0gNzJweCk7XG4gIHdpZHRoOjEwMCU7XG4gIC5jb250YWluZXJ7XG4gICAgZGlzcGxheTpmbGV4O1xuICAgIG1hdC1jYXJke1xuICAgICAgbWF4LXdpZHRoOjU1NnB4O1xuICAgICAgaGVpZ2h0OjEwMCU7XG4gICAgICBkaXNwbGF5OmJsb2NrO1xuICAgICAgbWFyZ2luOjEyMHB4IGF1dG87XG4gICAgICBwYWRkaW5nOjQwcHggMCA1NHB4O1xuICAgICAgbWF4LWhlaWdodDo0ODJweDtcbiAgICAgIHdpZHRoOjEwMCU7XG4gICAgfVxuXG5cbiAgICAubGVmdC1zcGFjZXtcbiAgICAgIHBhZGRpbmctbGVmdDoyNXB4O1xuICAgICAgd2lkdGg6Y2FsYygxMDAlIC0gMjVweCk7XG4gICAgfVxuICAgIC5yaWdodC1zcGFjZXtcbiAgICAgIHBhZGRpbmctcmlnaHQ6MjVweDtcbiAgICAgIHdpZHRoOmNhbGMoMTAwJSAtIDI1cHgpO1xuXG4gICAgfVxuXG5cblxuICAgIC5sb2dpbi1idXR0b257XG4gICAgICB3aWR0aDo4MCU7XG4gICAgICBtYXJnaW46MCBhdXRvO1xuICAgICAgaGVpZ2h0OjUzcHg7XG4gICAgICBkaXNwbGF5OmJsb2NrO1xuICAgICAgZm9udC1zaXplOjE2cHg7XG4gICAgICBmb250LXdlaWdodDpib2xkO1xuICAgICAgZm9udC1mYW1pbHk6J0xhdG8nLHNhbnMtc2VyaWY7XG4gICAgICBAaW5jbHVkZSBib3gtc2hhZG93KCAwIDEwcHggMjBweCAwIHJnYmEoMTcwLCAyMDIsIDY1LCAwLjUpKTtcbiAgICB9XG4gICAgbWF0LWZvcm0tZmllbGR7XG4gICAgICB3aWR0aDoxMDAlO1xuICAgIH1cblxuXG4gICAgLm1pZGRsZS1jYXJkLC50b3AtY2FyZCwuYm90dG9tLWNhcmR7XG4gICAgICB3aWR0aDozMjhweDtcbiAgICAgIGRpc3BsYXk6YmxvY2s7XG4gICAgICBtYXJnaW46YXV0bztcbiAgICB9XG4gICAgLm1pZGRsZS1jYXJke1xuICAgICAgbWFyZ2luOjM1cHggYXV0byA3MHB4O1xuICAgIH1cblxuICAgIG1hdC1mb3JtLWZpZWxke1xuICAgICAgJjpmaXJzdC1jaGlsZHtcbiAgICAgICAgbWFyZ2luLWJvdHRvbToyMHB4O1xuICAgICAgfVxuICAgIH1cblxuICAgIGg0e1xuICAgICAgZm9udC1mYW1pbHk6J0xhdG8nLHNhbnMtc2VyaWY7XG4gICAgICBmb250LXNpemU6MThweDtcbiAgICAgIHBhZGRpbmc6MDtcbiAgICAgIG1hcmdpbjowO1xuICAgIH1cblxuICAgIGg2e1xuICAgICAgZm9udC1mYW1pbHk6J0xhdG8nLHNhbnMtc2VyaWY7XG4gICAgICBmb250LXNpemU6MTZweDtcbiAgICAgIG1hcmdpbjowO1xuICAgICAgbWFyZ2luLXRvcDoxNHB4O1xuICAgICAgZm9udC13ZWlnaHQ6Ym9sZDtcbiAgICB9XG4gIH1cbn1cbiIsIkBtaXhpbiBjb21tb25CZygkd2lkdGg6MTAwJSl7XG4gIHdpZHRoOiR3aWR0aDtcbiAgYmFja2dyb3VuZC1zaXplOmNvdmVyO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOmNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6bm8tcmVwZWF0O1xufVxuXG5cbkBtaXhpbiBib3JkZXItcmFkaXVzKCR2YWx1ZSl7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czokdmFsdWU7XG4gIC1tb3otYm9yZGVyLXJhZGl1czokdmFsdWU7XG4gIGJvcmRlci1yYWRpdXM6JHZhbHVlO1xufVxuQG1peGluIGJveC1zaGFkb3coJHZhbHVlKXtcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAkdmFsdWU7XG4gIC1tb3otYm94LXNoYWRvdzogJHZhbHVlO1xuICBib3gtc2hhZG93OiAkdmFsdWU7XG59XG5cblxuQG1peGluIGN1c3RvbS10cmFuc2l0aW9uKCR2YWx1ZSl7XG4gIHRyYW5zaXRpb246ICR2YWx1ZTtcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiAkdmFsdWU7XG4gIC1tb3otdHJhbnNpdGlvbjogJHZhbHVlO1xuICAtbXMtdHJhbnNpdGlvbjogJHZhbHVlO1xufVxuXG5AbWl4aW4gY3VzdG9tLXRyYW5zZm9ybSgkdmFsdWUpe1xuICAtd2Via2l0LXRyYW5zZm9ybTogJHZhbHVlO1xuICAtbW96LXRyYW5zZm9ybTogJHZhbHVlO1xuICAtbXMtdHJhbnNmb3JtOiAkdmFsdWU7XG4gIC1vLXRyYW5zZm9ybTogJHZhbHVlO1xuICB0cmFuc2Zvcm06ICR2YWx1ZTtcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/auth/register/register.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/auth/register/register.component.ts ***!
  \*****************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _helpers_auth_helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../helpers/auth.helper */ "./src/app/auth/helpers/auth.helper.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(authHelper) {
        this.authHelper = authHelper;
        this.passwordType = 'password';
        this._isError = false;
        this._statusMsg = {};
        this.signupForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            firstname: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(2)]),
            lastname: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(2)]),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(2)])
        });
    }
    RegisterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authHelper.onAuthStatus.subscribe(function (r) {
            if (r && r.id === _this.regId && r.response) {
                if (r.response.statusCode > 301) {
                    _this._isError = true;
                    _this._statusMsg = r.response.data;
                }
                else {
                    _this.signupForm.reset();
                    _this._statusMsg = {};
                    alert(r.response.statusText);
                }
            }
        });
    };
    RegisterComponent.prototype.togglePasswordType = function () {
        if (this.passwordType === 'password') {
            this.passwordType = 'text';
        }
        else {
            this.passwordType = 'password';
        }
    };
    RegisterComponent.prototype.signup = function () {
        this.regId = Math.random();
        this.authHelper.fakeForm = this.signupForm;
        this.authHelper.register(this.regId);
    };
    RegisterComponent.prototype.preventDefault = function (event) {
        event.preventDefault();
    };
    Object.defineProperty(RegisterComponent.prototype, "isError", {
        get: function () {
            return this._isError;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RegisterComponent.prototype, "statusMsg", {
        get: function () {
            return this._statusMsg;
        },
        enumerable: true,
        configurable: true
    });
    RegisterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! ./register.component.html */ "./src/app/auth/register/register.component.html"),
            styles: [__webpack_require__(/*! ./register.component.scss */ "./src/app/auth/register/register.component.scss")]
        }),
        __metadata("design:paramtypes", [_helpers_auth_helper__WEBPACK_IMPORTED_MODULE_2__["AuthHelper"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ })

}]);
//# sourceMappingURL=auth-auth-module.js.map