import { Component, OnInit } from '@angular/core';
import {AuthHelper} from '../../auth/helpers/auth.helper.js';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  // public passwordType = 'password';
  // private _isError = false;
  // private loginId: any = {} as any;
  // private _statusMsg: any = {} as any;
  // public loginForm: FormGroup = new FormGroup({
  //   email: new FormControl('', [Validators.required, Validators.email]),
  //   password: new FormControl('', [Validators.required, Validators.minLength(2)])
  // });
  constructor(private authHelper: AuthHelper, private router: Router) {
    if (!this.authHelper.isLoggedIn()) {
      this.router.navigate(['auth/login']);
    }
  }

  ngOnInit() {

  }

  public togglePasswordType() {

  }

  public login() {

  }

  public preventDefault(event) {
    event.preventDefault();
  }


  // get isError(): boolean {
  //   return this._isError;
  // }
  //
  // get statusMsg(): any {
  //   return this._statusMsg;
  // }
}
