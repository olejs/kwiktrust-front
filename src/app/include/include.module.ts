import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import {MatGridListModule} from "@angular/material";
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [HeaderComponent, FooterComponent],
  entryComponents:  [],
  imports: [
    CommonModule,
    RouterModule,
    MatGridListModule,
  ],
  exports:  [HeaderComponent, FooterComponent]
})
export class IncludeModule { }
