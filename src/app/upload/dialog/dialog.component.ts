import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MatDatepicker } from '@angular/material';
import { UploadService } from '../upload.service';
import { forkJoin } from 'rxjs';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css'],
})

export class DialogComponent {
  @ViewChild('file') file;
  public files: Set<File> = new Set();
  public progress;
  public canBeClosed = true;
  public primaryButtonText = 'Upload';
  public showCancelButton = true;
  public uploading = false;
  public uploadSuccessful = false;
  public size = false;
  public timestamp = null;

  constructor(public dialogRef: MatDialogRef<DialogComponent>, public uploadService: UploadService) {}

  addFiles() {
    this.file.nativeElement.click();
  }

  onFilesAdded() {
    this.canBeClosed = false;
    const files: { [key: string]: File } = this.file.nativeElement.files;

    if (files) {
      this.size = true;
    }

    for (let key in files) {
      if (!isNaN(parseInt(key))) {
        this.files.add(files[key]);
      }
    }
  }

  addDate(type: string, event: MatDatepickerInputEvent<Date>) {
    const timeStamp = new Date(event.value);
    this.timestamp = timeStamp.getTime();
    this.canBeClosed = true;
  }

  closeDialog() {

    // if everything was uploaded already, just close the dialog
    if (this.uploadSuccessful) {
      return this.dialogRef.close();
    }

    // set the component state to "uploading"
    this.uploading = true;

    // start the upload and save the progress map
    this.progress = this.uploadService.upload(this.files, this.timestamp);

    // convert the progress map into an array
    let allProgressObservables = [];
    for (let key in this.progress) {
      allProgressObservables.push(this.progress[key].progress);
    }

    // Adjust the state variables

    // The OK-button should have the text "Finish" now
    this.primaryButtonText = 'Finish';

    // The dialog should not be closed while uploading
    this.canBeClosed = false;
    this.dialogRef.disableClose = true;

    // Hide the cancel-button
    this.showCancelButton = false;

    // When all progress-observables are completed...
    forkJoin(allProgressObservables).subscribe(end => {
      // ... the dialog can be closed again...
      this.canBeClosed = true;
      this.dialogRef.disableClose = false;

      // ... the upload was successful...
      this.uploadSuccessful = true;

      // ... and the component is no longer uploading
      this.uploading = false;
    });
  }
}
