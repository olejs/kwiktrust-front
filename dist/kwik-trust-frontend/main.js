(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-SG": "./node_modules/moment/locale/en-SG.js",
	"./en-SG.js": "./node_modules/moment/locale/en-SG.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./auth/auth.module": [
		"./src/app/auth/auth.module.ts",
		"auth-auth-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return __webpack_require__.e(ids[1]).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/Components/Dashboard/dashboard.component.html":
/*!***************************************************************!*\
  !*** ./src/app/Components/Dashboard/dashboard.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<section [class.bg-bg]=\"true\">\n  <div class=\"container\">\n    <div class=\"list\">\n      <app-upload></app-upload>\n    </div>\n  </div>\n</section>\n<app-footer></app-footer>\n\n"

/***/ }),

/***/ "./src/app/Components/Dashboard/dashboard.component.js":
/*!*************************************************************!*\
  !*** ./src/app/Components/Dashboard/dashboard.component.js ***!
  \*************************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_helpers_auth_helper_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../auth/helpers/auth.helper.js */ "./src/app/auth/helpers/auth.helper.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DashboardComponent = /** @class */ (function () {
    // public passwordType = 'password';
    // private _isError = false;
    // private loginId: any = {} as any;
    // private _statusMsg: any = {} as any;
    // public loginForm: FormGroup = new FormGroup({
    //   email: new FormControl('', [Validators.required, Validators.email]),
    //   password: new FormControl('', [Validators.required, Validators.minLength(2)])
    // });
    function DashboardComponent(authHelper, router) {
        this.authHelper = authHelper;
        this.router = router;
        if (!this.authHelper.isLoggedIn()) {
            this.router.navigate(['auth/login']);
        }
    }
    DashboardComponent.prototype.ngOnInit = function () {
    };
    DashboardComponent.prototype.togglePasswordType = function () {
    };
    DashboardComponent.prototype.login = function () {
    };
    DashboardComponent.prototype.preventDefault = function (event) {
        event.preventDefault();
    };
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/Components/Dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.scss */ "./src/app/Components/Dashboard/dashboard.component.scss")]
        }),
        __metadata("design:paramtypes", [_auth_helpers_auth_helper_js__WEBPACK_IMPORTED_MODULE_1__["AuthHelper"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], DashboardComponent);
    return DashboardComponent;
}());

//# sourceMappingURL=dashboard.component.js.map

/***/ }),

/***/ "./src/app/Components/Dashboard/dashboard.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/Components/Dashboard/dashboard.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "app-footer {\n  position: fixed;\n  width: 100%;\n  bottom: 29px; }\n\n.tile-add {\n  background-color: white; }\n\n.tile {\n  width: 220px;\n  height: 220px;\n  background-color: white; }\n\n.list {\n  margin-top: 20px;\n  width: 100%; }\n\nsection {\n  height: calc(100% - 72px);\n  width: 100%; }\n\nsection .container {\n    display: flex; }\n\nsection .container mat-card {\n      max-width: 556px;\n      height: 100%;\n      display: block;\n      margin: 120px auto;\n      padding: 40px 0;\n      max-height: 360px;\n      width: 100%; }\n\nsection .container .login-button {\n      width: 80%;\n      margin: 0 auto;\n      display: block;\n      font-size: 16px;\n      font-weight: bold;\n      font-family: 'Lato',sans-serif;\n      box-shadow: 0 10px 20px 0 rgba(170, 202, 65, 0.5); }\n\nsection .container mat-form-field {\n      width: 100%; }\n\nsection .container .middle-card, section .container .top-card, section .container .bottom-card {\n      width: 328px;\n      display: block;\n      margin: auto; }\n\nsection .container .middle-card {\n      margin: 68px auto 40px; }\n\nsection .container mat-form-field:first-child {\n      margin-bottom: 20px; }\n\nsection .container h4 {\n      font-family: 'Lato',sans-serif;\n      font-size: 18px;\n      padding: 0;\n      margin: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29sZWpzL1Byb2plY3RzL2tlbHdpLWZyb250ZW5kL3NyYy9hcHAvQ29tcG9uZW50cy9EYXNoYm9hcmQvZGFzaGJvYXJkLmNvbXBvbmVudC5zY3NzIiwiL2hvbWUvb2xlanMvUHJvamVjdHMva2Vsd2ktZnJvbnRlbmQvc3JjL3RoZW1lL19taXhpbnMuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNFLGVBQWM7RUFDZCxXQUFVO0VBQ1YsWUFBVyxFQUFBOztBQUliO0VBSUUsdUJBQXVCLEVBQUE7O0FBR3pCO0VBQ0UsWUFBVztFQUNYLGFBQVk7RUFFWix1QkFBdUIsRUFBQTs7QUFHekI7RUFDRSxnQkFBZ0I7RUFDaEIsV0FBVyxFQUFBOztBQUdiO0VBQ0UseUJBQXdCO0VBQ3hCLFdBQVUsRUFBQTs7QUFGWjtJQUlJLGFBQVksRUFBQTs7QUFKaEI7TUFNTSxnQkFBZTtNQUNmLFlBQVc7TUFDWCxjQUFhO01BQ2Isa0JBQWlCO01BQ2pCLGVBQWM7TUFDZCxpQkFBZ0I7TUFDaEIsV0FBVSxFQUFBOztBQVpoQjtNQW9CTSxVQUFTO01BQ1QsY0FBYTtNQUNiLGNBQWE7TUFDYixlQUFjO01BQ2QsaUJBQWdCO01BQ2hCLDhCQUE2QjtNQ3JDakMsaUREc0M4RCxFQUFBOztBQTFCaEU7TUE2Qk0sV0FBVSxFQUFBOztBQTdCaEI7TUFrQ00sWUFBVztNQUNYLGNBQWE7TUFDYixZQUFXLEVBQUE7O0FBcENqQjtNQXVDTSxzQkFBcUIsRUFBQTs7QUF2QzNCO01BNENRLG1CQUFrQixFQUFBOztBQTVDMUI7TUFpRE0sOEJBQTZCO01BQzdCLGVBQWM7TUFDZCxVQUFTO01BQ1QsU0FBUSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvQ29tcG9uZW50cy9EYXNoYm9hcmQvZGFzaGJvYXJkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCAnLi4vLi4vLi4vdGhlbWUvbWl4aW5zJztcblxuYXBwLWZvb3RlcntcbiAgcG9zaXRpb246Zml4ZWQ7XG4gIHdpZHRoOjEwMCU7XG4gIGJvdHRvbToyOXB4O1xuXG59XG5cbi50aWxlLWFkZCB7XG4gIC8vd2lkdGg6MTIwcHg7XG4gIC8vaGVpZ2h0OjEyMHB4O1xuICAvL3BhZGRpbmc6IDUwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuXG4udGlsZSB7XG4gIHdpZHRoOjIyMHB4O1xuICBoZWlnaHQ6MjIwcHg7XG4gIC8vaGVpZ2h0OiBhdXRvO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cblxuLmxpc3Qge1xuICBtYXJnaW4tdG9wOiAyMHB4O1xuICB3aWR0aDogMTAwJTtcbn1cblxuc2VjdGlvbntcbiAgaGVpZ2h0OmNhbGMoMTAwJSAtIDcycHgpO1xuICB3aWR0aDoxMDAlO1xuICAuY29udGFpbmVye1xuICAgIGRpc3BsYXk6ZmxleDtcbiAgICBtYXQtY2FyZHtcbiAgICAgIG1heC13aWR0aDo1NTZweDtcbiAgICAgIGhlaWdodDoxMDAlO1xuICAgICAgZGlzcGxheTpibG9jaztcbiAgICAgIG1hcmdpbjoxMjBweCBhdXRvO1xuICAgICAgcGFkZGluZzo0MHB4IDA7XG4gICAgICBtYXgtaGVpZ2h0OjM2MHB4O1xuICAgICAgd2lkdGg6MTAwJTtcbiAgICB9XG5cblxuXG5cblxuICAgIC5sb2dpbi1idXR0b257XG4gICAgICB3aWR0aDo4MCU7XG4gICAgICBtYXJnaW46MCBhdXRvO1xuICAgICAgZGlzcGxheTpibG9jaztcbiAgICAgIGZvbnQtc2l6ZToxNnB4O1xuICAgICAgZm9udC13ZWlnaHQ6Ym9sZDtcbiAgICAgIGZvbnQtZmFtaWx5OidMYXRvJyxzYW5zLXNlcmlmO1xuICAgICAgQGluY2x1ZGUgYm94LXNoYWRvdyggMCAxMHB4IDIwcHggMCByZ2JhKDE3MCwgMjAyLCA2NSwgMC41KSk7XG4gICAgfVxuICAgIG1hdC1mb3JtLWZpZWxke1xuICAgICAgd2lkdGg6MTAwJTtcbiAgICB9XG5cblxuICAgIC5taWRkbGUtY2FyZCwudG9wLWNhcmQsLmJvdHRvbS1jYXJke1xuICAgICAgd2lkdGg6MzI4cHg7XG4gICAgICBkaXNwbGF5OmJsb2NrO1xuICAgICAgbWFyZ2luOmF1dG87XG4gICAgfVxuICAgIC5taWRkbGUtY2FyZHtcbiAgICAgIG1hcmdpbjo2OHB4IGF1dG8gNDBweDtcbiAgICB9XG5cbiAgICBtYXQtZm9ybS1maWVsZHtcbiAgICAgICY6Zmlyc3QtY2hpbGR7XG4gICAgICAgIG1hcmdpbi1ib3R0b206MjBweDtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBoNHtcbiAgICAgIGZvbnQtZmFtaWx5OidMYXRvJyxzYW5zLXNlcmlmO1xuICAgICAgZm9udC1zaXplOjE4cHg7XG4gICAgICBwYWRkaW5nOjA7XG4gICAgICBtYXJnaW46MDtcbiAgICB9XG4gIH1cbn1cbiIsIkBtaXhpbiBjb21tb25CZygkd2lkdGg6MTAwJSl7XG4gIHdpZHRoOiR3aWR0aDtcbiAgYmFja2dyb3VuZC1zaXplOmNvdmVyO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOmNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6bm8tcmVwZWF0O1xufVxuXG5cbkBtaXhpbiBib3JkZXItcmFkaXVzKCR2YWx1ZSl7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czokdmFsdWU7XG4gIC1tb3otYm9yZGVyLXJhZGl1czokdmFsdWU7XG4gIGJvcmRlci1yYWRpdXM6JHZhbHVlO1xufVxuQG1peGluIGJveC1zaGFkb3coJHZhbHVlKXtcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAkdmFsdWU7XG4gIC1tb3otYm94LXNoYWRvdzogJHZhbHVlO1xuICBib3gtc2hhZG93OiAkdmFsdWU7XG59XG5cblxuQG1peGluIGN1c3RvbS10cmFuc2l0aW9uKCR2YWx1ZSl7XG4gIHRyYW5zaXRpb246ICR2YWx1ZTtcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiAkdmFsdWU7XG4gIC1tb3otdHJhbnNpdGlvbjogJHZhbHVlO1xuICAtbXMtdHJhbnNpdGlvbjogJHZhbHVlO1xufVxuXG5AbWl4aW4gY3VzdG9tLXRyYW5zZm9ybSgkdmFsdWUpe1xuICAtd2Via2l0LXRyYW5zZm9ybTogJHZhbHVlO1xuICAtbW96LXRyYW5zZm9ybTogJHZhbHVlO1xuICAtbXMtdHJhbnNmb3JtOiAkdmFsdWU7XG4gIC1vLXRyYW5zZm9ybTogJHZhbHVlO1xuICB0cmFuc2Zvcm06ICR2YWx1ZTtcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _welcome_welcome_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./welcome/welcome.component */ "./src/app/welcome/welcome.component.ts");
/* harmony import */ var _Components_Dashboard_dashboard_component_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Components/Dashboard/dashboard.component.js */ "./src/app/Components/Dashboard/dashboard.component.js");
/* harmony import */ var _include_include_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./include/include.module */ "./src/app/include/include.module.ts");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _upload_upload_module_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./upload/upload.module.js */ "./src/app/upload/upload.module.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var routes = [{
        path: '',
        component: _welcome_welcome_component__WEBPACK_IMPORTED_MODULE_2__["WelcomeComponent"]
    }, {
        path: 'auth',
        loadChildren: './auth/auth.module#AuthModule'
    }, {
        path: 'dashboard',
        component: _Components_Dashboard_dashboard_component_js__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"]
    }];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_welcome_welcome_component__WEBPACK_IMPORTED_MODULE_2__["WelcomeComponent"], _Components_Dashboard_dashboard_component_js__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"]],
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes),
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__["FlexLayoutModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatGridListModule"],
                _include_include_module__WEBPACK_IMPORTED_MODULE_4__["IncludeModule"],
                _upload_upload_module_js__WEBPACK_IMPORTED_MODULE_7__["UploadModule"]
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-progress-bar *ngIf=\"apiProvider.inprogress\"  mode=\"indeterminate\" [value]=\"apiProvider.progress(0)\"></mat-progress-bar>\n\n\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-progress-bar {\n  position: fixed;\n  z-index: 10000; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29sZWpzL1Byb2plY3RzL2tlbHdpLWZyb250ZW5kL3NyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZUFBYztFQUNkLGNBQWEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIm1hdC1wcm9ncmVzcy1iYXJ7XG4gIHBvc2l0aW9uOmZpeGVkO1xuICB6LWluZGV4OjEwMDAwO1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _provider_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./provider/api.service */ "./src/app/provider/api.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(apiProvider) {
        this.apiProvider = apiProvider;
        this.title = 'kwik-trust-frontend';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        __metadata("design:paramtypes", [_provider_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiProvider"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]
            ],
            imports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatNativeDateModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatIconModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatProgressBarModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auth/helpers/auth.helper.js":
/*!*********************************************!*\
  !*** ./src/app/auth/helpers/auth.helper.js ***!
  \*********************************************/
/*! exports provided: AuthHelper */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthHelper", function() { return AuthHelper; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _provider_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../provider/api.service */ "./src/app/provider/api.service.ts");
/* harmony import */ var rxjs_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/index */ "./node_modules/rxjs/index.js");
/* harmony import */ var rxjs_index__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(rxjs_index__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AuthHelper = /** @class */ (function () {
    function AuthHelper(api, router) {
        this.api = api;
        this.router = router;
        this._onAuthStatus = new rxjs_index__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
    }
    AuthHelper.prototype.login = function (id) {
        var _this = this;
        if (!this.fakeForm) {
            console.error('Form value is required');
            return;
        }
        this.api.getUrl('auth/login', this.fakeForm.value, 'post', false).subscribe(function (r) {
            if (r && r.statusText) {
                _this.setSession(r.data);
                _this._onAuthStatus.next({ id: id, response: r });
            }
        }, function (error) {
            _this._onAuthStatus.next({ id: id, response: error });
        });
    };
    AuthHelper.prototype.register = function (id) {
        var _this = this;
        if (!this.fakeForm) {
            console.error('Form value is required');
            return;
        }
        this.api.getUrl('auth/signup', this.fakeForm.value, 'post', false).subscribe(function (r) {
            if (r && r.statusText) {
                _this._onAuthStatus.next({ id: id, response: r });
            }
        }, function (error) {
            _this._onAuthStatus.next({ id: id, response: error });
        });
    };
    Object.defineProperty(AuthHelper.prototype, "onAuthStatus", {
        get: function () {
            return this._onAuthStatus;
        },
        enumerable: true,
        configurable: true
    });
    AuthHelper.prototype.setSession = function (authResult) {
        var expiresAt = moment__WEBPACK_IMPORTED_MODULE_4__().add(authResult.expires, 'second');
        localStorage.setItem('token', authResult.token);
        localStorage.setItem('expires', JSON.stringify(expiresAt.valueOf()));
    };
    AuthHelper.prototype.getExpiration = function () {
        var expiration = localStorage.getItem('expires');
        var expiresAt = JSON.parse(expiration);
        return moment__WEBPACK_IMPORTED_MODULE_4__(expiresAt);
    };
    AuthHelper.prototype.isLoggedIn = function () {
        return moment__WEBPACK_IMPORTED_MODULE_4__().isBefore(this.getExpiration());
    };
    AuthHelper.prototype.isLoggedOut = function () {
        return !this.isLoggedIn();
    };
    AuthHelper.prototype.logout = function () {
        localStorage.removeItem('token');
        localStorage.removeItem('expires');
    };
    AuthHelper = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_provider_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiProvider"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], AuthHelper);
    return AuthHelper;
}());

//# sourceMappingURL=auth.helper.js.map

/***/ }),

/***/ "./src/app/auth/helpers/auth.helper.ts":
/*!*********************************************!*\
  !*** ./src/app/auth/helpers/auth.helper.ts ***!
  \*********************************************/
/*! exports provided: AuthHelper */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthHelper", function() { return AuthHelper; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _provider_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../provider/api.service */ "./src/app/provider/api.service.ts");
/* harmony import */ var rxjs_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/index */ "./node_modules/rxjs/index.js");
/* harmony import */ var rxjs_index__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(rxjs_index__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AuthHelper = /** @class */ (function () {
    function AuthHelper(api, router) {
        this.api = api;
        this.router = router;
        this._onAuthStatus = new rxjs_index__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
    }
    AuthHelper.prototype.login = function (id) {
        var _this = this;
        if (!this.fakeForm) {
            console.error('Form value is required');
            return;
        }
        this.api.getUrl('auth/login', this.fakeForm.value, 'post', false).subscribe(function (r) {
            if (r && r.statusText) {
                _this.setSession(r.data);
                _this._onAuthStatus.next({ id: id, response: r });
            }
        }, function (error) {
            _this._onAuthStatus.next({ id: id, response: error });
        });
    };
    AuthHelper.prototype.register = function (id) {
        var _this = this;
        if (!this.fakeForm) {
            console.error('Form value is required');
            return;
        }
        this.api.getUrl('auth/signup', this.fakeForm.value, 'post', false).subscribe(function (r) {
            if (r && r.statusText) {
                _this._onAuthStatus.next({ id: id, response: r });
            }
        }, function (error) {
            _this._onAuthStatus.next({ id: id, response: error });
        });
    };
    Object.defineProperty(AuthHelper.prototype, "onAuthStatus", {
        get: function () {
            return this._onAuthStatus;
        },
        enumerable: true,
        configurable: true
    });
    AuthHelper.prototype.setSession = function (authResult) {
        var expiresAt = moment__WEBPACK_IMPORTED_MODULE_4__().add(authResult.expires, 'second');
        localStorage.setItem('token', authResult.token);
        localStorage.setItem('expires', JSON.stringify(expiresAt.valueOf()));
    };
    AuthHelper.prototype.getExpiration = function () {
        var expiration = localStorage.getItem('expires');
        var expiresAt = JSON.parse(expiration);
        return moment__WEBPACK_IMPORTED_MODULE_4__(expiresAt);
    };
    AuthHelper.prototype.isLoggedIn = function () {
        return moment__WEBPACK_IMPORTED_MODULE_4__().isBefore(this.getExpiration());
    };
    AuthHelper.prototype.isLoggedOut = function () {
        return !this.isLoggedIn();
    };
    AuthHelper.prototype.logout = function () {
        localStorage.removeItem('token');
        localStorage.removeItem('expires');
    };
    AuthHelper = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_provider_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiProvider"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], AuthHelper);
    return AuthHelper;
}());



/***/ }),

/***/ "./src/app/config/app.configuration.ts":
/*!*********************************************!*\
  !*** ./src/app/config/app.configuration.ts ***!
  \*********************************************/
/*! exports provided: ApiConfiguration */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiConfiguration", function() { return ApiConfiguration; });
var host = 'localhost';
var port = '3400';
var scheme = 'http';
var ApiConfiguration = {
    endpoint: scheme + "://" + host + ":" + port + "/api/"
};


/***/ }),

/***/ "./src/app/include/footer/footer.component.html":
/*!******************************************************!*\
  !*** ./src/app/include/footer/footer.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer>\n  <div class=\"container\">\n    <div class=\"footer-tag-line\">\n      <a [class.text]=\"true\">Terms & Conditions</a>\n      <a [class.text]=\"true\">Privacy Policy</a>\n    </div>\n\n    <button class=\"chat-button\" [class.transparent-bg]=\"true\"\n            [class.no-border]=\"true\" [class.no-outline]=\"true\">\n      <i class=\"custom-icon chat\"></i>\n    </button>\n  </div>\n</footer>\n"

/***/ }),

/***/ "./src/app/include/footer/footer.component.scss":
/*!******************************************************!*\
  !*** ./src/app/include/footer/footer.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container {\n  position: relative; }\n  .container .footer-tag-line {\n    text-align: center; }\n  .container .footer-tag-line a {\n      font-family: 'Lato',sans-serif;\n      font-size: 10px;\n      font-weight: bold; }\n  .container .footer-tag-line a:first-child {\n        margin-right: 5px; }\n  .container .footer-tag-line a:last-child {\n        margin-left: 5px; }\n  .container .chat-button {\n    position: absolute;\n    right: 0;\n    bottom: 0; }\n  .container .chat-button .custom-icon {\n      width: 94px;\n      height: 94px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29sZWpzL1Byb2plY3RzL2tlbHdpLWZyb250ZW5kL3NyYy9hcHAvaW5jbHVkZS9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0Usa0JBQWlCLEVBQUE7RUFEbkI7SUFHSSxrQkFBaUIsRUFBQTtFQUhyQjtNQUtNLDhCQUE2QjtNQUM3QixlQUFjO01BQ2QsaUJBQWlCLEVBQUE7RUFQdkI7UUFVUSxpQkFBZ0IsRUFBQTtFQVZ4QjtRQWFRLGdCQUFlLEVBQUE7RUFidkI7SUFrQkksa0JBQWlCO0lBQ2pCLFFBQU87SUFDUCxTQUFRLEVBQUE7RUFwQlo7TUFzQk0sV0FBVTtNQUNWLFlBQVcsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2luY2x1ZGUvZm9vdGVyL2Zvb3Rlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuLmNvbnRhaW5lcntcbiAgcG9zaXRpb246cmVsYXRpdmU7XG4gIC5mb290ZXItdGFnLWxpbmV7XG4gICAgdGV4dC1hbGlnbjpjZW50ZXI7XG4gICAgYXtcbiAgICAgIGZvbnQtZmFtaWx5OidMYXRvJyxzYW5zLXNlcmlmO1xuICAgICAgZm9udC1zaXplOjEwcHg7XG4gICAgICBmb250LXdlaWdodDogYm9sZDtcblxuICAgICAgJjpmaXJzdC1jaGlsZHtcbiAgICAgICAgbWFyZ2luLXJpZ2h0OjVweDtcbiAgICAgIH1cbiAgICAgICY6bGFzdC1jaGlsZHtcbiAgICAgICAgbWFyZ2luLWxlZnQ6NXB4O1xuICAgICAgfVxuICAgIH1cbiAgfVxuICAuY2hhdC1idXR0b257XG4gICAgcG9zaXRpb246YWJzb2x1dGU7XG4gICAgcmlnaHQ6MDtcbiAgICBib3R0b206MDtcbiAgICAuY3VzdG9tLWljb257XG4gICAgICB3aWR0aDo5NHB4O1xuICAgICAgaGVpZ2h0Ojk0cHg7XG4gICAgfVxuICB9XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/include/footer/footer.component.ts":
/*!****************************************************!*\
  !*** ./src/app/include/footer/footer.component.ts ***!
  \****************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/include/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.scss */ "./src/app/include/footer/footer.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/include/header/header.component.html":
/*!******************************************************!*\
  !*** ./src/app/include/header/header.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header>\n  <div class=\"container\">\n    <mat-grid-list cols=\"4\" rowHeight=\"72px\">\n      <mat-grid-tile colspan=\"1\">\n        <div class=\"logo-container\">\n          <img src=\"/assets/img/icon/logo.png\" [routerLink]=\"'/'\"/>\n        </div>\n      </mat-grid-tile>\n      <mat-grid-tile colspan=\"3\" >\n        <div *ngIf=\"!isLogged; else loggedOut;\" class=\"action-buttons\">\n          <button [routerLink]=\"'/auth/signup'\" mat-button [class.round]=\"true\" [class.light]=\"true\" [class.login-button]=\"true\" [class.secondary-bg]=\"true\">SIGN UP</button>\n          <button [routerLink]=\"'/auth/login'\" mat-button [class.login-button]=\"true\" [class.transparent-bg]=\"true\" [class.light]=\"true\">LOG IN</button>\n        </div>\n        <ng-template #loggedOut>\n          <div class=\"action-buttons\">\n            <button (click)=\"logOut()\" mat-button [class.round]=\"true\" [class.light]=\"true\" [class.login-button]=\"true\" [class.secondary-bg]=\"true\">LOG OUT</button>\n          </div>\n        </ng-template>\n      </mat-grid-tile>\n    </mat-grid-list>\n  </div>\n  <div class=\"clearfix\"></div>\n</header>\n"

/***/ }),

/***/ "./src/app/include/header/header.component.scss":
/*!******************************************************!*\
  !*** ./src/app/include/header/header.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".transparent-bg {\n  background: transparent; }\n\n.primary {\n  color: #242e47 !important; }\n\n.primary-bg {\n  background: #242e47 !important; }\n\n.light {\n  color: #ffffff !important; }\n\n.light-bg {\n  background: #ffffff !important; }\n\n.bg {\n  color: #f5f7fa !important; }\n\n.bg-bg {\n  background: #f5f7fa !important; }\n\n.secondary {\n  color: #aaca41 !important; }\n\n.secondary-bg {\n  background: #aaca41 !important; }\n\n.text {\n  color: #1d2740 !important; }\n\n.text-bg {\n  background: #1d2740 !important; }\n\n.text-dark {\n  color: #1b2437 !important; }\n\n.text-dark-bg {\n  background: #1b2437 !important; }\n\n.text-light {\n  color: #a1a1a1 !important; }\n\n.text-light-bg {\n  background: #a1a1a1 !important; }\n\n.line-color {\n  color: #d4d4d4 !important; }\n\n.line-color-bg {\n  background: #d4d4d4 !important; }\n\n.danger {\n  color: #f34848 !important; }\n\n.danger-bg {\n  background: #f34848 !important; }\n\nheader {\n  background: #242e47; }\n\nheader .container .logo-container {\n    position: absolute;\n    left: 0; }\n\nheader .container .logo-container img {\n      width: 56px; }\n\nheader .container .action-buttons {\n    position: absolute;\n    right: 0; }\n\nheader .container .action-buttons button {\n      font-size: 12px;\n      font-family: 'SFProText Bold',sans-serif;\n      font-weight: bold;\n      font-style: normal;\n      font-stretch: normal;\n      line-height: normal;\n      letter-spacing: normal;\n      text-align: center; }\n\nheader .container .action-buttons button:first-child {\n        margin-right: 10px;\n        box-shadow: 0 10px 20px 0 rgba(170, 202, 65, 0.2); }\n\nheader .clearfix {\n    clear: both; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29sZWpzL1Byb2plY3RzL2tlbHdpLWZyb250ZW5kL3NyYy90aGVtZS9fY29sb3JzLnNjc3MiLCIvaG9tZS9vbGVqcy9Qcm9qZWN0cy9rZWx3aS1mcm9udGVuZC9zcmMvYXBwL2luY2x1ZGUvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFjQTtFQUNFLHVCQUFzQixFQUFBOztBQUd0QjtFQUNFLHlCQUF1QixFQUFBOztBQUV6QjtFQUNFLDhCQUE0QixFQUFBOztBQUo5QjtFQUNFLHlCQUF1QixFQUFBOztBQUV6QjtFQUNFLDhCQUE0QixFQUFBOztBQUo5QjtFQUNFLHlCQUF1QixFQUFBOztBQUV6QjtFQUNFLDhCQUE0QixFQUFBOztBQUo5QjtFQUNFLHlCQUF1QixFQUFBOztBQUV6QjtFQUNFLDhCQUE0QixFQUFBOztBQUo5QjtFQUNFLHlCQUF1QixFQUFBOztBQUV6QjtFQUNFLDhCQUE0QixFQUFBOztBQUo5QjtFQUNFLHlCQUF1QixFQUFBOztBQUV6QjtFQUNFLDhCQUE0QixFQUFBOztBQUo5QjtFQUNFLHlCQUF1QixFQUFBOztBQUV6QjtFQUNFLDhCQUE0QixFQUFBOztBQUo5QjtFQUNFLHlCQUF1QixFQUFBOztBQUV6QjtFQUNFLDhCQUE0QixFQUFBOztBQUo5QjtFQUNFLHlCQUF1QixFQUFBOztBQUV6QjtFQUNFLDhCQUE0QixFQUFBOztBQ2xCaEM7RUFDRSxtQkRKZSxFQUFBOztBQ0dqQjtJQUlNLGtCQUFpQjtJQUNqQixPQUFNLEVBQUE7O0FBTFo7TUFPUSxXQUFVLEVBQUE7O0FBUGxCO0lBY0ssa0JBQWlCO0lBQ2hCLFFBQU8sRUFBQTs7QUFmYjtNQWlCUSxlQUFjO01BQ2Qsd0NBQXVDO01BQ3ZDLGlCQUFnQjtNQUNoQixrQkFBa0I7TUFDbEIsb0JBQW9CO01BQ3BCLG1CQUFtQjtNQUNuQixzQkFBc0I7TUFDdEIsa0JBQWlCLEVBQUE7O0FBeEJ6QjtRQTBCVSxrQkFBaUI7UUFDakIsaURBQWlELEVBQUE7O0FBM0IzRDtJQWtDSSxXQUFVLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9pbmNsdWRlL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIkY29sb3JzOihcbiAgcHJpbWFyeTojMjQyZTQ3LFxuICBsaWdodDojZmZmZmZmLFxuICBiZzojZjVmN2ZhLFxuICBzZWNvbmRhcnk6I2FhY2E0MSxcbiAgdGV4dDojMWQyNzQwLFxuICB0ZXh0LWRhcms6IzFiMjQzNyxcbiAgdGV4dC1saWdodDojYTFhMWExLFxuICBsaW5lLWNvbG9yOiNkNGQ0ZDQsXG4gIGRhbmdlcjojZjM0ODQ4XG4pO1xuXG5cblxuLnRyYW5zcGFyZW50LWJne1xuICBiYWNrZ3JvdW5kOnRyYW5zcGFyZW50O1xufVxuQGVhY2ggJG5hbWUsJHZhbHVlIGluICRjb2xvcnN7XG4gIC4jeyRuYW1lfXtcbiAgICBjb2xvcjokdmFsdWUgIWltcG9ydGFudFxuICB9XG4gIC4jeyRuYW1lfS1iZ3tcbiAgICBiYWNrZ3JvdW5kOiR2YWx1ZSAhaW1wb3J0YW50XG4gIH1cblxufVxuIiwiQGltcG9ydCBcIi4uLy4uLy4uL3RoZW1lL2NvbG9yc1wiO1xuXG5cblxuaGVhZGVye1xuICBiYWNrZ3JvdW5kOm1hcF9nZXQoJGNvbG9ycyxwcmltYXJ5KTtcbiAgLmNvbnRhaW5lcntcbiAgICAubG9nby1jb250YWluZXJ7XG4gICAgICBwb3NpdGlvbjphYnNvbHV0ZTtcbiAgICAgIGxlZnQ6MDtcbiAgICAgIGltZ3tcbiAgICAgICAgd2lkdGg6NTZweDtcbiAgICAgIH1cblxuICAgIH1cblxuXG4gICAgLmFjdGlvbi1idXR0b25ze1xuICAgICBwb3NpdGlvbjphYnNvbHV0ZTtcbiAgICAgIHJpZ2h0OjA7XG4gICAgICBidXR0b257XG4gICAgICAgIGZvbnQtc2l6ZToxMnB4O1xuICAgICAgICBmb250LWZhbWlseTonU0ZQcm9UZXh0IEJvbGQnLHNhbnMtc2VyaWY7XG4gICAgICAgIGZvbnQtd2VpZ2h0OmJvbGQ7XG4gICAgICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgICAgICAgZm9udC1zdHJldGNoOiBub3JtYWw7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XG4gICAgICAgIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XG4gICAgICAgIHRleHQtYWxpZ246Y2VudGVyO1xuICAgICAgICAmOmZpcnN0LWNoaWxke1xuICAgICAgICAgIG1hcmdpbi1yaWdodDoxMHB4O1xuICAgICAgICAgIGJveC1zaGFkb3c6IDAgMTBweCAyMHB4IDAgcmdiYSgxNzAsIDIwMiwgNjUsIDAuMik7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAuY2xlYXJmaXh7XG4gICAgY2xlYXI6Ym90aDtcbiAgfVxufVxuIl19 */"

/***/ }),

/***/ "./src/app/include/header/header.component.ts":
/*!****************************************************!*\
  !*** ./src/app/include/header/header.component.ts ***!
  \****************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_helpers_auth_helper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../auth/helpers/auth.helper */ "./src/app/auth/helpers/auth.helper.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(authHelper, ngZone) {
        this.authHelper = authHelper;
        this.ngZone = ngZone;
    }
    HeaderComponent.prototype.ngOnInit = function () {
        this.isLogged = this.authHelper.isLoggedIn();
    };
    HeaderComponent.prototype.logOut = function () {
        this.authHelper.logout();
    };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/include/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/include/header/header.component.scss")]
        }),
        __metadata("design:paramtypes", [_auth_helpers_auth_helper__WEBPACK_IMPORTED_MODULE_1__["AuthHelper"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/include/include.module.ts":
/*!*******************************************!*\
  !*** ./src/app/include/include.module.ts ***!
  \*******************************************/
/*! exports provided: IncludeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IncludeModule", function() { return IncludeModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./header/header.component */ "./src/app/include/header/header.component.ts");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/include/footer/footer.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var IncludeModule = /** @class */ (function () {
    function IncludeModule() {
    }
    IncludeModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_header_header_component__WEBPACK_IMPORTED_MODULE_2__["HeaderComponent"], _footer_footer_component__WEBPACK_IMPORTED_MODULE_3__["FooterComponent"]],
            entryComponents: [],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatGridListModule"],
            ],
            exports: [_header_header_component__WEBPACK_IMPORTED_MODULE_2__["HeaderComponent"], _footer_footer_component__WEBPACK_IMPORTED_MODULE_3__["FooterComponent"]]
        })
    ], IncludeModule);
    return IncludeModule;
}());



/***/ }),

/***/ "./src/app/provider/api.service.ts":
/*!*****************************************!*\
  !*** ./src/app/provider/api.service.ts ***!
  \*****************************************/
/*! exports provided: ApiProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiProvider", function() { return ApiProvider; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/index */ "./node_modules/rxjs/index.js");
/* harmony import */ var rxjs_index__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(rxjs_index__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/internal/operators */ "./node_modules/rxjs/internal/operators/index.js");
/* harmony import */ var rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _token_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./token.provider */ "./src/app/provider/token.provider.ts");
/* harmony import */ var _config_app_configuration__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../config/app.configuration */ "./src/app/config/app.configuration.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ApiProvider = /** @class */ (function () {
    function ApiProvider(http, tokenProvider) {
        this.http = http;
        this.tokenProvider = tokenProvider;
        this._progress = 0;
        this._loading = false;
        this._progressId = 0;
        this._inprogress = false;
        this._allProgress = {};
    }
    ApiProvider.prototype.ping = function (url, data, type, secured, json, headers) {
        var _this = this;
        this._loading = true;
        url = url.startsWith('http') ? url : (_config_app_configuration__WEBPACK_IMPORTED_MODULE_5__["ApiConfiguration"].endpoint + url);
        console.log(data);
        console.log(url);
        console.log(type);
        this._inprogress = true;
        headers = headers ? headers : {};
        if (secured) {
            headers['Authorization'] = this.tokenProvider.token;
            console.log('header');
            console.log(headers);
        }
        if (json) {
            headers['Content-Type'] = 'application/json';
        }
        var options = {
            reportProgress: true,
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"](headers)
        };
        console.log(options);
        var req = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpRequest"](type.toUpperCase(), url, data, options);
        return this.http.request(req).pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_3__["filter"])(function (event) {
            console.log(event);
            var progress = _this.getEventMessage(event);
            if (progress > 0) {
                _this._progress = progress;
            }
            if (event.type === _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].Response) {
                return event;
            }
        }), Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            _this._loading = false;
            _this._progress = 0;
            console.log(event);
            _this._inprogress = false;
            return res.body;
        }), Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(function (error, caught) {
            _this._loading = false;
            console.log(error);
            console.log(caught);
            if (error && error.error) {
                error = error.error;
            }
            _this._inprogress = false;
            return Object(rxjs_index__WEBPACK_IMPORTED_MODULE_2__["throwError"])(error);
        }));
    };
    ApiProvider.prototype.pingNoHeader = function (url, data, type) {
        var _this = this;
        this._loading = true;
        url = url.startsWith('http') ? url : (_config_app_configuration__WEBPACK_IMPORTED_MODULE_5__["ApiConfiguration"].endpoint + url);
        this._inprogress = true;
        var req = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpRequest"](type.toUpperCase(), url, data);
        return this.http.request(req).pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_3__["filter"])(function (event) {
            console.log(event);
            var progress = _this.getEventMessage(event);
            if (progress > 0) {
                _this._progress = progress;
            }
            if (event.type === _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].Response) {
                return event;
            }
        }), Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            _this._loading = false;
            _this._progress = 0;
            console.log(event);
            _this._inprogress = false;
            return res.body;
        }), Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(function (error, caught) {
            _this._loading = false;
            console.log(error);
            console.log(caught);
            if (error && error.error) {
                error = error.error;
            }
            _this._inprogress = false;
            return Object(rxjs_index__WEBPACK_IMPORTED_MODULE_2__["throwError"])(error);
        }));
    };
    ApiProvider.prototype.getEventMessage = function (event) {
        switch (event.type) {
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].Sent:
                return 0;
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].UploadProgress:
                var progress = Math.round(100 * event.loaded / event.total);
                this._allProgress[this._progressId] = progress;
                return progress;
            case _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].Response:
                this._allProgress[this._progressId] = 100;
                return 100;
            default:
                return;
        }
    };
    ApiProvider.prototype.progress = function (btnText) {
        if ((this._progress < 100 && this._progress > 0)) {
            return this._progress;
        }
        else {
            return btnText;
        }
    };
    Object.defineProperty(ApiProvider.prototype, "loading", {
        get: function () {
            return this._loading;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApiProvider.prototype, "progressId", {
        get: function () {
            return this._progressId;
        },
        set: function (value) {
            this._progressId = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ApiProvider.prototype, "allProgress", {
        get: function () {
            return this._allProgress;
        },
        set: function (value) {
            this._allProgress = value;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Ping function
     *
     * @param url
     * @param body
     * @param type
     * @param isProtected
     * @param header
     */
    ApiProvider.prototype.getUrl = function (url, body, type, isProtected, header) {
        var _this = this;
        if (!header) {
            header = {};
        }
        if (isProtected) {
            header['Authorization'] = "" + this.tokenProvider.token;
        }
        var headers = this.setHeader(header);
        if (type == 'get' || type == 'delete') {
            body = { headers: headers };
        }
        var urlPrefix = !url.startsWith('http') ? _config_app_configuration__WEBPACK_IMPORTED_MODULE_5__["ApiConfiguration"].endpoint : '';
        console.log('################### API SERVICE #####################');
        console.log('url is ' + urlPrefix + url);
        console.log('body is ');
        console.log(body);
        console.log('request type is ' + type);
        console.log('headers');
        console.log(headers);
        console.log('###################END API SERVICE######################');
        this._inprogress = true;
        return this.http[type](urlPrefix + url, body, { headers: headers }).pipe(Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            _this._loading = false;
            _this._progress = 0;
            _this._inprogress = false;
            console.log('####RES####');
            console.log(res);
            console.log('####EORES####');
            return res;
        }), Object(rxjs_internal_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(function (error, caught) {
            _this._loading = false;
            var dat = error;
            console.log('error response');
            console.log(dat);
            if (dat && dat.error) {
                dat = error.error;
            }
            _this._inprogress = false;
            return Object(rxjs_index__WEBPACK_IMPORTED_MODULE_2__["throwError"])(dat);
        }));
    };
    /**
     * Set header function
     *
     * @param headers
     * @return {HttpHeaders}
     */
    ApiProvider.prototype.setHeader = function (headers) {
        return new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"](headers);
    };
    Object.defineProperty(ApiProvider.prototype, "inprogress", {
        get: function () {
            return this._inprogress;
        },
        enumerable: true,
        configurable: true
    });
    ApiProvider = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _token_provider__WEBPACK_IMPORTED_MODULE_4__["TokenProvider"]])
    ], ApiProvider);
    return ApiProvider;
}());



/***/ }),

/***/ "./src/app/provider/token.provider.ts":
/*!********************************************!*\
  !*** ./src/app/provider/token.provider.ts ***!
  \********************************************/
/*! exports provided: TokenProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenProvider", function() { return TokenProvider; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TokenProvider = /** @class */ (function () {
    function TokenProvider() {
    }
    TokenProvider.prototype.setup = function () {
    };
    TokenProvider.prototype.setToken = function (token) {
    };
    Object.defineProperty(TokenProvider.prototype, "token", {
        get: function () {
            return this._token;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TokenProvider.prototype, "user", {
        get: function () {
            return this._user;
        },
        enumerable: true,
        configurable: true
    });
    TokenProvider = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], TokenProvider);
    return TokenProvider;
}());



/***/ }),

/***/ "./src/app/upload/dialog/dialog.component.css":
/*!****************************************************!*\
  !*** ./src/app/upload/dialog/dialog.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".add-files-btn {\n  float: right;\n}\n\n:host {\n  height: 100%;\n  display: flex;\n  flex: 1;\n  flex-direction: column;\n}\n\n.actions {\n  justify-content: flex-end;\n}\n\n.container {\n  height: 100%;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXBsb2FkL2RpYWxvZy9kaWFsb2cuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFlBQVk7QUFDZDs7QUFFQTtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IsT0FBTztFQUNQLHNCQUFzQjtBQUN4Qjs7QUFFQTtFQUNFLHlCQUF5QjtBQUMzQjs7QUFFQTtFQUNFLFlBQVk7QUFDZCIsImZpbGUiOiJzcmMvYXBwL3VwbG9hZC9kaWFsb2cvZGlhbG9nLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYWRkLWZpbGVzLWJ0biB7XG4gIGZsb2F0OiByaWdodDtcbn1cblxuOmhvc3Qge1xuICBoZWlnaHQ6IDEwMCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXg6IDE7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG59XG5cbi5hY3Rpb25zIHtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbn1cblxuLmNvbnRhaW5lciB7XG4gIGhlaWdodDogMTAwJTtcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/upload/dialog/dialog.component.html":
/*!*****************************************************!*\
  !*** ./src/app/upload/dialog/dialog.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<input\n  type=\"file\"\n  #file\n  style=\"display: none\"\n  (change)=\"onFilesAdded()\"\n  multiple\n/>\n<div class=\"container\" fxLayout=\"column\" fxLayoutAlign=\"space-evenly stretch\">\n  <h1 mat-dialog-title style=\"text-align: center;\">Upload File</h1>\n    <button\n      [disabled]=\"uploading || uploadSuccessful || size\"\n      mat-raised-button\n      style=\"background-color: #aaca41; color: white; font-weight: bold; max-width: 132px; margin:auto;\"\n      class=\"add-files-btn\"\n      (click)=\"addFiles()\"\n    >\n      Add File\n    </button>\n\n  <!-- This is the content of the dialog, containing a list of the files to upload -->\n  <mat-dialog-content fxFlex>\n    <mat-list>\n      <mat-list-item *ngFor=\"let file of files\">\n        <h4 mat-line>{{file.name}}</h4>\n        <mat-progress-bar\n          *ngIf=\"progress\"\n          mode=\"determinate\"\n          [value]=\"progress[file.name].progress | async\"\n        ></mat-progress-bar>\n      </mat-list-item>\n    </mat-list>\n    <div *ngIf=\"size\">\n      <input matInput [matDatepicker]=\"myDatepicker\" placeholder=\"Choose expiration date\" (dateChange)=\"addDate('change', $event)\">\n      <mat-datepicker-toggle matSuffix [for]=\"myDatepicker\"></mat-datepicker-toggle>\n      <mat-datepicker #myDatepicker></mat-datepicker>\n    </div>\n  </mat-dialog-content>\n\n  <!-- This are the actions of the dialog, containing the primary and the cancel button-->\n  <mat-dialog-actions class=\"actions\">\n    <button mat-button mat-dialog-close>Cancel</button>\n    <button\n      mat-raised-button\n      style=\"background-color: #aaca41; color: white; font-weight: bold;\"\n      [disabled]=\"!canBeClosed\"\n      (click)=\"closeDialog()\"\n    >\n      {{primaryButtonText}}\n    </button>\n  </mat-dialog-actions>\n</div>\n"

/***/ }),

/***/ "./src/app/upload/dialog/dialog.component.ts":
/*!***************************************************!*\
  !*** ./src/app/upload/dialog/dialog.component.ts ***!
  \***************************************************/
/*! exports provided: DialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DialogComponent", function() { return DialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _upload_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../upload.service */ "./src/app/upload/upload.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DialogComponent = /** @class */ (function () {
    function DialogComponent(dialogRef, uploadService) {
        this.dialogRef = dialogRef;
        this.uploadService = uploadService;
        this.files = new Set();
        this.canBeClosed = true;
        this.primaryButtonText = 'Upload';
        this.showCancelButton = true;
        this.uploading = false;
        this.uploadSuccessful = false;
        this.size = false;
        this.timestamp = null;
    }
    DialogComponent.prototype.addFiles = function () {
        this.file.nativeElement.click();
    };
    DialogComponent.prototype.onFilesAdded = function () {
        this.canBeClosed = false;
        var files = this.file.nativeElement.files;
        if (files) {
            this.size = true;
        }
        for (var key in files) {
            if (!isNaN(parseInt(key))) {
                this.files.add(files[key]);
            }
        }
    };
    DialogComponent.prototype.addDate = function (type, event) {
        var timeStamp = new Date(event.value);
        this.timestamp = timeStamp.getTime();
        this.canBeClosed = true;
    };
    DialogComponent.prototype.closeDialog = function () {
        var _this = this;
        // if everything was uploaded already, just close the dialog
        if (this.uploadSuccessful) {
            return this.dialogRef.close();
        }
        // set the component state to "uploading"
        this.uploading = true;
        // start the upload and save the progress map
        this.progress = this.uploadService.upload(this.files, this.timestamp);
        // convert the progress map into an array
        var allProgressObservables = [];
        for (var key in this.progress) {
            allProgressObservables.push(this.progress[key].progress);
        }
        // Adjust the state variables
        // The OK-button should have the text "Finish" now
        this.primaryButtonText = 'Finish';
        // The dialog should not be closed while uploading
        this.canBeClosed = false;
        this.dialogRef.disableClose = true;
        // Hide the cancel-button
        this.showCancelButton = false;
        // When all progress-observables are completed...
        Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["forkJoin"])(allProgressObservables).subscribe(function (end) {
            // ... the dialog can be closed again...
            _this.canBeClosed = true;
            _this.dialogRef.disableClose = false;
            // ... the upload was successful...
            _this.uploadSuccessful = true;
            // ... and the component is no longer uploading
            _this.uploading = false;
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('file'),
        __metadata("design:type", Object)
    ], DialogComponent.prototype, "file", void 0);
    DialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dialog',
            template: __webpack_require__(/*! ./dialog.component.html */ "./src/app/upload/dialog/dialog.component.html"),
            styles: [__webpack_require__(/*! ./dialog.component.css */ "./src/app/upload/dialog/dialog.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], _upload_service__WEBPACK_IMPORTED_MODULE_2__["UploadService"]])
    ], DialogComponent);
    return DialogComponent;
}());



/***/ }),

/***/ "./src/app/upload/upload.component.html":
/*!**********************************************!*\
  !*** ./src/app/upload/upload.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"tile-add\">\n  <button mat-raised-button (click)=\"openUploadDialog()\">\n    <img width=\"120px\" height=\"120px\" src=\"./../../assets/img/icon/document-add.svg\">\n  </button>\n</div>\n"

/***/ }),

/***/ "./src/app/upload/upload.component.scss":
/*!**********************************************!*\
  !*** ./src/app/upload/upload.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-progress-bar {\n  position: fixed;\n  z-index: 10000; }\n\n.tile-add {\n  width: 120px;\n  height: 120px;\n  background-color: white; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29sZWpzL1Byb2plY3RzL2tlbHdpLWZyb250ZW5kL3NyYy9hcHAvdXBsb2FkL3VwbG9hZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGVBQWM7RUFDZCxjQUFhLEVBQUE7O0FBR2Y7RUFDRSxZQUFXO0VBQ1gsYUFBWTtFQUNaLHVCQUF1QixFQUFBIiwiZmlsZSI6InNyYy9hcHAvdXBsb2FkL3VwbG9hZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIm1hdC1wcm9ncmVzcy1iYXJ7XG4gIHBvc2l0aW9uOmZpeGVkO1xuICB6LWluZGV4OjEwMDAwO1xufVxuXG4udGlsZS1hZGQge1xuICB3aWR0aDoxMjBweDtcbiAgaGVpZ2h0OjEyMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/upload/upload.component.ts":
/*!********************************************!*\
  !*** ./src/app/upload/upload.component.ts ***!
  \********************************************/
/*! exports provided: UploadComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadComponent", function() { return UploadComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _dialog_dialog_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dialog/dialog.component */ "./src/app/upload/dialog/dialog.component.ts");
/* harmony import */ var _upload_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./upload.service */ "./src/app/upload/upload.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UploadComponent = /** @class */ (function () {
    function UploadComponent(dialog, uploadService) {
        this.dialog = dialog;
        this.uploadService = uploadService;
    }
    UploadComponent.prototype.openUploadDialog = function () {
        var dialogRef = this.dialog.open(_dialog_dialog_component__WEBPACK_IMPORTED_MODULE_2__["DialogComponent"], {
            width: '50%',
            height: '50%',
        });
    };
    UploadComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-upload',
            template: __webpack_require__(/*! ./upload.component.html */ "./src/app/upload/upload.component.html"),
            styles: [__webpack_require__(/*! ./upload.component.scss */ "./src/app/upload/upload.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"], _upload_service__WEBPACK_IMPORTED_MODULE_3__["UploadService"]])
    ], UploadComponent);
    return UploadComponent;
}());



/***/ }),

/***/ "./src/app/upload/upload.module.js":
/*!*****************************************!*\
  !*** ./src/app/upload/upload.module.js ***!
  \*****************************************/
/*! exports provided: UploadModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadModule", function() { return UploadModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _upload_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./upload.component */ "./src/app/upload/upload.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _dialog_dialog_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dialog/dialog.component */ "./src/app/upload/dialog/dialog.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _upload_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./upload.service */ "./src/app/upload/upload.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var UploadModule = /** @class */ (function () {
    function UploadModule() {
    }
    UploadModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatListModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__["FlexLayoutModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClientModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatProgressBarModule"],
            ],
            declarations: [_upload_component__WEBPACK_IMPORTED_MODULE_2__["UploadComponent"], _dialog_dialog_component__WEBPACK_IMPORTED_MODULE_4__["DialogComponent"]],
            exports: [_upload_component__WEBPACK_IMPORTED_MODULE_2__["UploadComponent"]],
            entryComponents: [_dialog_dialog_component__WEBPACK_IMPORTED_MODULE_4__["DialogComponent"]],
            providers: [_upload_service__WEBPACK_IMPORTED_MODULE_7__["UploadService"]],
        })
    ], UploadModule);
    return UploadModule;
}());

//# sourceMappingURL=upload.module.js.map

/***/ }),

/***/ "./src/app/upload/upload.service.ts":
/*!******************************************!*\
  !*** ./src/app/upload/upload.service.ts ***!
  \******************************************/
/*! exports provided: UploadService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadService", function() { return UploadService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var url = 'http://localhost:3400/api/upload';
var UploadService = /** @class */ (function () {
    function UploadService(http) {
        this.http = http;
    }
    UploadService.prototype.upload = function (files, timestamp) {
        var _this = this;
        // this will be the our resulting map
        var status = {};
        files.forEach(function (file) {
            // create a new multipart-form for every file
            var formData = new FormData();
            formData.append('file', file, file.name);
            formData.append('timestamp', timestamp);
            // create a http-post request and pass the form   thi
            // tell it to report the upload progress
            var req = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpRequest"]('POST', url, formData, {
                reportProgress: true
            });
            // create a new progress-subject for every file
            var progress = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
            // send the http-request and subscribe for progress-updates
            _this.http.request(req).subscribe(function (event) {
                if (event.type === _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].UploadProgress) {
                    // calculate the progress percentage
                    var percentDone = Math.round(100 * event.loaded / event.total);
                    // pass the percentage into the progress-stream
                    progress.next(percentDone);
                }
                else if (event instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpResponse"]) {
                    //  Close the progress-stream if we get an answer form the API
                    // The upload is complete
                    progress.complete();
                }
                // error during uploading files
            }, function (error) {
                alert('Connection problem ' + error.toString());
            });
            // Save every progress-observable in a map of all observables
            status[file.name] = {
                progress: progress.asObservable()
            };
        });
        // return the map of progress.observables
        return status;
    };
    UploadService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], UploadService);
    return UploadService;
}());



/***/ }),

/***/ "./src/app/welcome/welcome.component.html":
/*!************************************************!*\
  !*** ./src/app/welcome/welcome.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<section [class.bg-bg]=\"true\">\n  <div class=\"container\">\n\n    <mat-grid-list cols=\"2\" rowHeight=\"100%\" [class.column-view]=\"true\">\n      <mat-grid-tile>\n        <div class=\"inner-grid\">\n          <div class=\"home-bg\"></div>\n\n        </div>\n      </mat-grid-tile>\n      <mat-grid-tile>\n        <div class=\"inner-grid\">\n          <div class=\"writeup\">\n\n            <h1 [class.text-dark]>Creating a Trusted Trading Environment</h1>\n            <p>Kwiktrust is a blockchain, smart-contract project that tackles the challenges of  B2B trade through creating a trusted trading environment.</p>\n            <p>A due diligence system that checks supplier references for you automatically, saving you time and money.</p>\n            <p>Makes use of artificial intelligence to make smart checks and blockchain technology to exchange and store data securely.</p>\n          </div>\n        </div>\n\n  </mat-grid-tile>\n    </mat-grid-list>\n\n  </div>\n</section>\n<app-footer></app-footer>\n"

/***/ }),

/***/ "./src/app/welcome/welcome.component.scss":
/*!************************************************!*\
  !*** ./src/app/welcome/welcome.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".transparent-bg {\n  background: transparent; }\n\n.primary {\n  color: #242e47 !important; }\n\n.primary-bg {\n  background: #242e47 !important; }\n\n.light {\n  color: #ffffff !important; }\n\n.light-bg {\n  background: #ffffff !important; }\n\n.bg {\n  color: #f5f7fa !important; }\n\n.bg-bg {\n  background: #f5f7fa !important; }\n\n.secondary {\n  color: #aaca41 !important; }\n\n.secondary-bg {\n  background: #aaca41 !important; }\n\n.text {\n  color: #1d2740 !important; }\n\n.text-bg {\n  background: #1d2740 !important; }\n\n.text-dark {\n  color: #1b2437 !important; }\n\n.text-dark-bg {\n  background: #1b2437 !important; }\n\n.text-light {\n  color: #a1a1a1 !important; }\n\n.text-light-bg {\n  background: #a1a1a1 !important; }\n\n.line-color {\n  color: #d4d4d4 !important; }\n\n.line-color-bg {\n  background: #d4d4d4 !important; }\n\n.danger {\n  color: #f34848 !important; }\n\n.danger-bg {\n  background: #f34848 !important; }\n\napp-footer {\n  position: fixed;\n  width: 100%;\n  bottom: 29px; }\n\n.home-bg {\n  width: 100%;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat;\n  background: url(\"/assets/img/background/home-bg.png\");\n  background-position: center;\n  background-repeat: no-repeat;\n  height: 100%; }\n\nsection {\n  height: calc(100% - 72px);\n  width: 100%; }\n\nsection .container {\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    height: 100%; }\n\nsection .container mat-grid-list {\n      max-width: 1200px;\n      margin: auto;\n      width: 100%; }\n\nsection .container .inner-grid {\n      height: 100%;\n      width: 100%;\n      max-width: 527px;\n      max-height: 668px; }\n\nsection .container .writeup {\n      height: 100%; }\n\nsection h1 {\n    font-size: 50px;\n    font-family: 'Lato',sans-serif;\n    font-weight: bold;\n    line-height: 1.2;\n    margin: 0; }\n\nsection p {\n    font-family: 'Lato',sans-serif;\n    font-size: 14px;\n    font-weight: bold;\n    margin-top: 25px;\n    color: #a1a1a1; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29sZWpzL1Byb2plY3RzL2tlbHdpLWZyb250ZW5kL3NyYy90aGVtZS9fY29sb3JzLnNjc3MiLCIvaG9tZS9vbGVqcy9Qcm9qZWN0cy9rZWx3aS1mcm9udGVuZC9zcmMvYXBwL3dlbGNvbWUvd2VsY29tZS5jb21wb25lbnQuc2NzcyIsIi9ob21lL29sZWpzL1Byb2plY3RzL2tlbHdpLWZyb250ZW5kL3NyYy90aGVtZS9fbWl4aW5zLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBY0E7RUFDRSx1QkFBc0IsRUFBQTs7QUFHdEI7RUFDRSx5QkFBdUIsRUFBQTs7QUFFekI7RUFDRSw4QkFBNEIsRUFBQTs7QUFKOUI7RUFDRSx5QkFBdUIsRUFBQTs7QUFFekI7RUFDRSw4QkFBNEIsRUFBQTs7QUFKOUI7RUFDRSx5QkFBdUIsRUFBQTs7QUFFekI7RUFDRSw4QkFBNEIsRUFBQTs7QUFKOUI7RUFDRSx5QkFBdUIsRUFBQTs7QUFFekI7RUFDRSw4QkFBNEIsRUFBQTs7QUFKOUI7RUFDRSx5QkFBdUIsRUFBQTs7QUFFekI7RUFDRSw4QkFBNEIsRUFBQTs7QUFKOUI7RUFDRSx5QkFBdUIsRUFBQTs7QUFFekI7RUFDRSw4QkFBNEIsRUFBQTs7QUFKOUI7RUFDRSx5QkFBdUIsRUFBQTs7QUFFekI7RUFDRSw4QkFBNEIsRUFBQTs7QUFKOUI7RUFDRSx5QkFBdUIsRUFBQTs7QUFFekI7RUFDRSw4QkFBNEIsRUFBQTs7QUFKOUI7RUFDRSx5QkFBdUIsRUFBQTs7QUFFekI7RUFDRSw4QkFBNEIsRUFBQTs7QUNuQmhDO0VBQ0UsZUFBYztFQUNkLFdBQVU7RUFDVixZQUFXLEVBQUE7O0FBSWI7RUNURSxXRFVzQjtFQ1R0QixzQkFBcUI7RUFDckIsMkJBQTBCO0VBQzFCLDRCQUEyQjtFRFEzQixxREFBb0Q7RUFDcEQsMkJBQTBCO0VBQzFCLDRCQUEyQjtFQUMzQixZQUFXLEVBQUE7O0FBRWI7RUFDRSx5QkFBeUI7RUFDekIsV0FBVyxFQUFBOztBQUZiO0lBSUksYUFBYTtJQUNiLHNCQUFxQjtJQUNyQix1QkFBc0I7SUFDdEIsWUFBVyxFQUFBOztBQVBmO01BU00saUJBQWdCO01BQ2hCLFlBQVc7TUFDWCxXQUFVLEVBQUE7O0FBWGhCO01BZU0sWUFBVztNQUNYLFdBQVU7TUFDVixnQkFBZ0I7TUFDaEIsaUJBQWlCLEVBQUE7O0FBbEJ2QjtNQXVCTSxZQUFXLEVBQUE7O0FBdkJqQjtJQTRCSSxlQUFjO0lBQ2QsOEJBQTZCO0lBQzdCLGlCQUFnQjtJQUNoQixnQkFBZTtJQUNmLFNBQVEsRUFBQTs7QUFoQ1o7SUFvQ0ksOEJBQTZCO0lBQzdCLGVBQWM7SUFDZCxpQkFBZ0I7SUFDaEIsZ0JBQWU7SUFDZixjRGxEZ0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3dlbGNvbWUvd2VsY29tZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiRjb2xvcnM6KFxuICBwcmltYXJ5OiMyNDJlNDcsXG4gIGxpZ2h0OiNmZmZmZmYsXG4gIGJnOiNmNWY3ZmEsXG4gIHNlY29uZGFyeTojYWFjYTQxLFxuICB0ZXh0OiMxZDI3NDAsXG4gIHRleHQtZGFyazojMWIyNDM3LFxuICB0ZXh0LWxpZ2h0OiNhMWExYTEsXG4gIGxpbmUtY29sb3I6I2Q0ZDRkNCxcbiAgZGFuZ2VyOiNmMzQ4NDhcbik7XG5cblxuXG4udHJhbnNwYXJlbnQtYmd7XG4gIGJhY2tncm91bmQ6dHJhbnNwYXJlbnQ7XG59XG5AZWFjaCAkbmFtZSwkdmFsdWUgaW4gJGNvbG9yc3tcbiAgLiN7JG5hbWV9e1xuICAgIGNvbG9yOiR2YWx1ZSAhaW1wb3J0YW50XG4gIH1cbiAgLiN7JG5hbWV9LWJne1xuICAgIGJhY2tncm91bmQ6JHZhbHVlICFpbXBvcnRhbnRcbiAgfVxuXG59XG4iLCJAaW1wb3J0ICcuLi8uLi90aGVtZS9taXhpbnMnO1xuQGltcG9ydCAnLi4vLi4vdGhlbWUvY29sb3JzJztcblxuYXBwLWZvb3RlcntcbiAgcG9zaXRpb246Zml4ZWQ7XG4gIHdpZHRoOjEwMCU7XG4gIGJvdHRvbToyOXB4O1xuXG59XG5cbi5ob21lLWJne1xuICBAaW5jbHVkZSBjb21tb25CZygxMDAlKTtcbiAgYmFja2dyb3VuZDp1cmwoXCIvYXNzZXRzL2ltZy9iYWNrZ3JvdW5kL2hvbWUtYmcucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOmNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6bm8tcmVwZWF0O1xuICBoZWlnaHQ6MTAwJTtcbn1cbnNlY3Rpb24ge1xuICBoZWlnaHQ6IGNhbGMoMTAwJSAtIDcycHgpO1xuICB3aWR0aDogMTAwJTtcbiAgLmNvbnRhaW5lciB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjpjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OmNlbnRlcjtcbiAgICBoZWlnaHQ6MTAwJTtcbiAgICBtYXQtZ3JpZC1saXN0e1xuICAgICAgbWF4LXdpZHRoOjEyMDBweDtcbiAgICAgIG1hcmdpbjphdXRvO1xuICAgICAgd2lkdGg6MTAwJTtcbiAgICB9XG5cbiAgICAuaW5uZXItZ3JpZHtcbiAgICAgIGhlaWdodDoxMDAlO1xuICAgICAgd2lkdGg6MTAwJTtcbiAgICAgIG1heC13aWR0aDogNTI3cHg7XG4gICAgICBtYXgtaGVpZ2h0OiA2NjhweDtcbiAgICB9XG5cblxuICAgIC53cml0ZXVwe1xuICAgICAgaGVpZ2h0OjEwMCU7XG4gICAgfVxuICB9XG5cbiAgaDF7XG4gICAgZm9udC1zaXplOjUwcHg7XG4gICAgZm9udC1mYW1pbHk6J0xhdG8nLHNhbnMtc2VyaWY7XG4gICAgZm9udC13ZWlnaHQ6Ym9sZDtcbiAgICBsaW5lLWhlaWdodDoxLjI7XG4gICAgbWFyZ2luOjA7XG4gIH1cblxuICBwe1xuICAgIGZvbnQtZmFtaWx5OidMYXRvJyxzYW5zLXNlcmlmO1xuICAgIGZvbnQtc2l6ZToxNHB4O1xuICAgIGZvbnQtd2VpZ2h0OmJvbGQ7XG4gICAgbWFyZ2luLXRvcDoyNXB4O1xuICAgIGNvbG9yOm1hcF9nZXQoJGNvbG9ycyx0ZXh0LWxpZ2h0KTtcbiAgfVxufVxuIiwiQG1peGluIGNvbW1vbkJnKCR3aWR0aDoxMDAlKXtcbiAgd2lkdGg6JHdpZHRoO1xuICBiYWNrZ3JvdW5kLXNpemU6Y292ZXI7XG4gIGJhY2tncm91bmQtcG9zaXRpb246Y2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDpuby1yZXBlYXQ7XG59XG5cblxuQG1peGluIGJvcmRlci1yYWRpdXMoJHZhbHVlKXtcbiAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiR2YWx1ZTtcbiAgLW1vei1ib3JkZXItcmFkaXVzOiR2YWx1ZTtcbiAgYm9yZGVyLXJhZGl1czokdmFsdWU7XG59XG5AbWl4aW4gYm94LXNoYWRvdygkdmFsdWUpe1xuICAtd2Via2l0LWJveC1zaGFkb3c6ICR2YWx1ZTtcbiAgLW1vei1ib3gtc2hhZG93OiAkdmFsdWU7XG4gIGJveC1zaGFkb3c6ICR2YWx1ZTtcbn1cblxuXG5AbWl4aW4gY3VzdG9tLXRyYW5zaXRpb24oJHZhbHVlKXtcbiAgdHJhbnNpdGlvbjogJHZhbHVlO1xuICAtd2Via2l0LXRyYW5zaXRpb246ICR2YWx1ZTtcbiAgLW1vei10cmFuc2l0aW9uOiAkdmFsdWU7XG4gIC1tcy10cmFuc2l0aW9uOiAkdmFsdWU7XG59XG5cbkBtaXhpbiBjdXN0b20tdHJhbnNmb3JtKCR2YWx1ZSl7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiAkdmFsdWU7XG4gIC1tb3otdHJhbnNmb3JtOiAkdmFsdWU7XG4gIC1tcy10cmFuc2Zvcm06ICR2YWx1ZTtcbiAgLW8tdHJhbnNmb3JtOiAkdmFsdWU7XG4gIHRyYW5zZm9ybTogJHZhbHVlO1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/welcome/welcome.component.ts":
/*!**********************************************!*\
  !*** ./src/app/welcome/welcome.component.ts ***!
  \**********************************************/
/*! exports provided: WelcomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WelcomeComponent", function() { return WelcomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WelcomeComponent = /** @class */ (function () {
    function WelcomeComponent() {
    }
    WelcomeComponent.prototype.ngOnInit = function () {
    };
    WelcomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-welcome',
            template: __webpack_require__(/*! ./welcome.component.html */ "./src/app/welcome/welcome.component.html"),
            styles: [__webpack_require__(/*! ./welcome.component.scss */ "./src/app/welcome/welcome.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], WelcomeComponent);
    return WelcomeComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/olejs/Projects/kelwi-frontend/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map