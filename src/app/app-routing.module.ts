import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WelcomeComponent} from './welcome/welcome.component';
import {DashboardComponent} from './Components/Dashboard/dashboard.component.js';
import {IncludeModule} from './include/include.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatGridListModule} from '@angular/material';
import {UploadModule} from './upload/upload.module.js';

const routes: Routes = [{
  path: '',
  component: WelcomeComponent
}, {
  path: 'auth',
  loadChildren: './auth/auth.module#AuthModule'
}, {
  path: 'dashboard',
  component: DashboardComponent
}];

@NgModule({
  declarations: [WelcomeComponent, DashboardComponent],
  imports: [
    RouterModule.forRoot(routes),
    FlexLayoutModule,
    MatGridListModule,
    IncludeModule,
    UploadModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
