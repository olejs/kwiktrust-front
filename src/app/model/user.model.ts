export class UserModel {
  public id:number;
  public firstname:string;
  public lastname:string;
  public email:string;
}
