import { Injectable } from '@angular/core';
import {HttpClient, HttpEvent, HttpEventType, HttpHeaders, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs/index';
import {catchError, filter, map} from 'rxjs/internal/operators';
import {TokenProvider} from './token.provider';
import {ApiConfiguration} from '../config/app.configuration';


@Injectable({
  providedIn: 'root'
})
export class ApiProvider {
  private _progress = 0;
  private _loading = false;
  private _progressId = 0;
  private _inprogress = false;
  private _allProgress = {};
  constructor(public http: HttpClient, private tokenProvider: TokenProvider) {

  }

  public ping(url: string, data: any, type: string, secured: boolean, json?: boolean, headers?: {}): Observable<any> {
    this._loading = true;
    url = url.startsWith('http') ? url : (ApiConfiguration.endpoint + url);
    console.log(data);
    console.log(url);
    console.log(type);
    this._inprogress = true;
    headers = headers ? headers : {};
    if (secured) {
      headers['Authorization'] = this.tokenProvider.token;
      console.log('header');
      console.log(headers);
    }
    if (json) {
      headers['Content-Type'] = 'application/json';
    }
    const options = {
      reportProgress: true,
      headers: new HttpHeaders(headers)
    };
    console.log(options);
    const req = new HttpRequest(type.toUpperCase(), url, data, options);
    return this.http.request(req).pipe(

        filter((event: any) => {
          console.log(event);
          const progress = this.getEventMessage(event);
          if (progress > 0) {this._progress = progress; }
          if (event.type === HttpEventType.Response) {
            return event;
          }
        }),
        map((res: Response) => {
          this._loading = false;
          this._progress = 0;
          console.log(event);
          this._inprogress = false;
          return res.body;
        }),
        catchError((error: any, caught: Observable<any>) => {
          this._loading = false;
          console.log(error);
          console.log(caught);
          if (error && error.error) {
            error = error.error;
          }
          this._inprogress = false;

          return throwError(error);
        })

    );


  }

  public pingNoHeader(url: string, data: any, type: string): Observable<any> {
    this._loading = true;
    url = url.startsWith('http') ? url : (ApiConfiguration.endpoint + url);

    this._inprogress = true;
    const req = new HttpRequest(type.toUpperCase(), url, data);
    return this.http.request(req).pipe(

        filter((event: any) => {
          console.log(event);
          const progress = this.getEventMessage(event);
          if (progress > 0) {this._progress = progress; }
          if (event.type === HttpEventType.Response) {
            return event;
          }
        }),
        map((res: Response) => {
          this._loading = false;
          this._progress = 0;
          console.log(event);
          this._inprogress = false;

          return res.body;
        }),
        catchError((error: any, caught: Observable<any>) => {
          this._loading = false;
          console.log(error);
          console.log(caught);
          if (error && error.error) {
            error = error.error;
          }
          this._inprogress = false;

          return throwError(error);
        })

    );


  }



  private getEventMessage(event: HttpEvent<any>) {
    switch (event.type) {
      case HttpEventType.Sent:
        return 0;
      case HttpEventType.UploadProgress:
        const progress = Math.round(100 * event.loaded / event.total);
        this._allProgress[this._progressId] = progress;
        return progress;
      case HttpEventType.Response:
        this._allProgress[this._progressId] = 100;
        return 100;
      default:
        return;
    }
  }


  public progress(btnText: number): number {
    if ((this._progress < 100 && this._progress > 0)) {
      return this._progress;
    } else {
      return btnText;
    }
  }

  get loading(): boolean {
    return this._loading;
  }


  get progressId(): number {
    return this._progressId;
  }

  set progressId(value: number) {
    this._progressId = value;
  }

  get allProgress(): {} {
    return this._allProgress;
  }

  set allProgress(value: {}) {
    this._allProgress = value;
  }








  /**
   * Ping function
   *
   * @param url
   * @param body
   * @param type
   * @param isProtected
   * @param header
   */
  public getUrl(url: string, body: any, type: string, isProtected: boolean, header?: {}) {
    if (!header) {
      header = {};
    }
    if (isProtected) {
      header['Authorization'] = `${this.tokenProvider.token}`;
    }
    const headers = this.setHeader(header);

    if (type == 'get' || type == 'delete') {
      body = {headers: headers};
    }
    const urlPrefix = !url.startsWith('http') ? ApiConfiguration.endpoint : '';

    console.log('################### API SERVICE #####################');
    console.log('url is ' + urlPrefix + url);
    console.log('body is ');
    console.log(body);
    console.log('request type is ' + type);
    console.log('headers');
    console.log(headers);
    console.log('###################END API SERVICE######################');

    this._inprogress = true;

    return this.http[type](urlPrefix + url, body, {headers: headers}).pipe(
        map((res: Response) => {
          this._loading = false;
          this._progress = 0;
          this._inprogress = false;
          console.log('####RES####');
          console.log(res);
          console.log('####EORES####');
          return res;
        }),
        catchError((error: any, caught: Observable<any>) => {
          this._loading = false;
          let dat = error;
          console.log('error response');
          console.log(dat);

          if (dat && dat.error) {
            dat = error.error;
          }
          this._inprogress = false;

          return throwError(dat);
        })
    );

  }

  /**
   * Set header function
   *
   * @param headers
   * @return {HttpHeaders}
   */
  private setHeader(headers: any): HttpHeaders {
    return new HttpHeaders(headers);
  }

  get inprogress(): boolean {
    return this._inprogress;
  }
}
