import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthHelper } from '../helpers/auth.helper';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public passwordType = 'password';
  private _isError = false;
  private loginId: any = {} as any;
  private _statusMsg: any = {} as any;
  public loginForm: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(2)])
  });

  constructor(private authHelper: AuthHelper,  private router: Router) {
    if (this.authHelper.isLoggedIn()) {
      this.router.navigate(['dashboard']);
    }
  }

  ngOnInit() {
    this.authHelper.onAuthStatus.subscribe(r => {
      if (r && r.id === this.loginId && r.response) {
        if (r.response.statusCode > 301) {
          this._isError = true;
          this._statusMsg = r.response.data;
        } else {
          this.loginForm.reset();
          this._statusMsg = {} as any;
          this.router.navigate(['dashboard']);
          // alert(r.response.statusText);
        }
      }
    });
  }

  public togglePasswordType() {
    if (this.passwordType === 'password') {
      this.passwordType = 'text';
    } else {
      this.passwordType = 'password';
    }
  }

  public login() {
    this.loginId = Math.random();
    this.authHelper.fakeForm = this.loginForm;
    this.authHelper.login(this.loginId);
  }

  public preventDefault(event) {
    event.preventDefault();
  }

  get isError(): boolean {
    return this._isError;
  }

  get statusMsg(): any {
    return this._statusMsg;
  }
}
