import { Component } from '@angular/core';
import {ApiProvider} from './provider/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'kwik-trust-frontend';

  constructor(public apiProvider: ApiProvider) {}
}
